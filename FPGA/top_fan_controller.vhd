Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

library machXO3;
use machXO3.all;

entity top_fan_controller is
  --generic (
  --  16  : integer := 16
  --);
  port (
    -- SPI
    spi_clk      : in  std_logic;
    spi_miso     : out std_logic;
    spi_mosi     : in  std_logic;
    spi_cs       : in  std_logic;

    -- Outputs
    ctrl_pwm_out : out std_logic_vector (16 - 1 downto 0);
    pwr_pwm_out  : out std_logic_vector (16 - 1 downto 0);
    pwr_en_out   : out std_logic_vector (16 - 1 downto 0)

  );
end entity;

architecture rtl of top_fan_controller is
  -- parameterized module component declaration
  component main_pll
      port (CLKI: in  std_logic; CLKOP: out  std_logic; 
          LOCK: out  std_logic);
  end component;

  COMPONENT OSCH
  -- synthesis translate_off
  GENERIC (NOM_FREQ: string := "133");
  -- synthesis translate_on
  PORT (STDBY : IN std_logic;
  OSC : OUT std_logic;
  SEDSTDBY : OUT std_logic);
  END COMPONENT;
  attribute NOM_FREQ : string;
  attribute NOM_FREQ of OSCinst0 : label is "133";

  signal pll_locked               : std_logic;
  signal clk                      : std_logic;
  signal oscillator               : std_logic;
  signal reset                    : std_logic;

  signal reg_high_speed_count_top : unsigned(15 downto 0);
  type reg_high_speed_pwm_val_T is array (16-1 downto 0) of unsigned(15 downto 0);
  signal reg_high_speed_pwm_val : reg_high_speed_pwm_val_T;

  signal high_speed_count         : unsigned(15 downto 0);

  begin

  -- FPGA's built in oscillator
  OSCInst0: OSCH
  -- synthesis translate_off
  GENERIC MAP(
    NOM_FREQ => "133"
  )
  -- synthesis translate_on
  PORT MAP (
    STDBY     => '0',
    OSC       => oscillator,
    SEDSTDBY  => open
  );

  -- PLL
  main_pll_inst : main_pll
  port map (
    CLKI=> oscillator,
    CLKOP=> clk,
    LOCK=> pll_locked
    );

  reset <= not pll_locked;

  reg_high_speed_pwm_val(0) <= x"CAFE";
  reg_high_speed_pwm_val(1) <= x"CAFE";
  reg_high_speed_pwm_val(2) <= x"CAFE";
  reg_high_speed_pwm_val(3) <= x"CAFE";
  reg_high_speed_pwm_val(4) <= x"CAFE";
  reg_high_speed_pwm_val(5) <= x"CAFE";
  reg_high_speed_pwm_val(6) <= x"CAFE";
  reg_high_speed_pwm_val(7) <= x"CAFE";
  reg_high_speed_pwm_val(8) <= x"CAFE";
  reg_high_speed_pwm_val(9) <= x"CAFE";
  reg_high_speed_pwm_val(10) <= x"CAFE";
  reg_high_speed_pwm_val(11) <= x"CAFE";
  reg_high_speed_pwm_val(12) <= x"CAFE";
  reg_high_speed_pwm_val(13) <= x"CAFE";
  reg_high_speed_pwm_val(14) <= x"CAFE";
  reg_high_speed_pwm_val(15) <= x"CAFE";

  -- High frequency PWM
  process(clk, reset) is
    begin
      if reset = '1' then
        high_speed_count <= (others => '0');
        pwr_pwm_out      <= (others => '0');

      elsif rising_edge(clk) then
        
        -- Counter
        if high_speed_count /= reg_high_speed_count_top then
          high_speed_count <= high_speed_count + 1;
        else 
          high_speed_count <= (others => '0');
        end if;

        -- PWM output
        for i in 0 to 16 - 1 loop
          if reg_high_speed_pwm_val(i) = x"0000" then
            pwr_pwm_out(i) <= '0';
          elsif high_speed_count < reg_high_speed_pwm_val(i) then
            pwr_pwm_out(i) <= '1';
          else 
            pwr_pwm_out(i) <= '0';
          end if;

        end loop;

      end if;
  end process;


end architecture;
