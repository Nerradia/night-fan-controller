EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 18 18
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2900 5250 0    50   ~ 0
usb+
Text Label 2900 5350 0    50   ~ 0
usb-
Wire Wire Line
	2900 5250 3550 5250
Wire Wire Line
	3550 5350 2900 5350
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5FDA3195
P 3750 5250
F 0 "J?" H 3830 5242 50  0000 L CNN
F 1 "USB" H 3830 5151 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3750 5250 50  0001 C CNN
F 3 "~" H 3750 5250 50  0001 C CNN
	1    3750 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA319B
P 3450 5550
F 0 "#PWR?" H 3450 5300 50  0001 C CNN
F 1 "GND" H 3455 5377 50  0000 C CNN
F 2 "" H 3450 5550 50  0001 C CNN
F 3 "" H 3450 5550 50  0001 C CNN
	1    3450 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 5550 3450 5450
Wire Wire Line
	3450 5450 3550 5450
Wire Wire Line
	3500 -1250 3500 -1350
Wire Wire Line
	3500 -500 3500 -950
Text Label 3500 -500 1    50   ~ 0
SDA
$Comp
L Device:R_Pack04_Split RN?
U 2 1 5FDA31A6
P 3500 -1100
AR Path="/5FBA8D0D/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCC8244/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCC8817/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCC8DBD/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCC9264/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCC979C/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCC9B94/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCCA014/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FCCA439/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F63/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F69/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F6F/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F75/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F7B/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F81/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F87/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F8D/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FD39F93/5FDA31A6" Ref="RN?"  Part="1" 
AR Path="/5FDA31A6" Ref="RN?"  Part="2" 
F 0 "RN?" H 3588 -1054 50  0000 L CNN
F 1 "4.7k" H 3588 -1145 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0603" V 3420 -1100 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 3500 -1100 50  0001 C CNN
	2    3500 -1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 -1250 3050 -1350
Wire Wire Line
	3050 -500 3050 -950
Text Label 3050 -500 1    50   ~ 0
SCL
$Comp
L Device:R_Pack04_Split RN?
U 1 1 5FDA31AF
P 3050 -1100
AR Path="/5FBA8D0D/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCC8244/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCC8817/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCC8DBD/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCC9264/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCC979C/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCC9B94/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCCA014/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FCCA439/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F63/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F69/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F6F/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F75/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F7B/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F81/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F87/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F8D/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FD39F93/5FDA31AF" Ref="RN?"  Part="1" 
AR Path="/5FDA31AF" Ref="RN?"  Part="1" 
F 0 "RN?" H 3138 -1054 50  0000 L CNN
F 1 "4.7k" H 3138 -1145 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0603" V 2970 -1100 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 3050 -1100 50  0001 C CNN
	1    3050 -1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 6750 3100 6750
Wire Wire Line
	2950 6850 2950 6750
Wire Wire Line
	2950 7150 2950 7250
$Comp
L power:GND #PWR?
U 1 1 5FDA31B8
P 2950 7250
F 0 "#PWR?" H 2950 7000 50  0001 C CNN
F 1 "GND" V 2955 7122 50  0000 R CNN
F 2 "" H 2950 7250 50  0001 C CNN
F 3 "" H 2950 7250 50  0001 C CNN
	1    2950 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FDA31BE
P 2950 7000
F 0 "C?" V 2698 7000 50  0000 C CNN
F 1 "1u" V 2789 7000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2988 6850 50  0001 C CNN
F 3 "~" H 2950 7000 50  0001 C CNN
	1    2950 7000
	-1   0    0    1   
$EndComp
Connection ~ 3550 6750
Wire Wire Line
	3400 6750 3550 6750
Wire Wire Line
	3650 6750 3550 6750
Text GLabel 3650 6750 2    50   Output ~ 0
+3.3VA
Wire Wire Line
	3550 7150 3550 7250
Wire Wire Line
	3550 6750 3550 6850
$Comp
L power:GND #PWR?
U 1 1 5FDA31CA
P 3550 7250
F 0 "#PWR?" H 3550 7000 50  0001 C CNN
F 1 "GND" V 3555 7122 50  0000 R CNN
F 2 "" H 3550 7250 50  0001 C CNN
F 3 "" H 3550 7250 50  0001 C CNN
	1    3550 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FDA31D0
P 3550 7000
F 0 "C?" V 3298 7000 50  0000 C CNN
F 1 "1u" V 3389 7000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3588 6850 50  0001 C CNN
F 3 "~" H 3550 7000 50  0001 C CNN
	1    3550 7000
	-1   0    0    1   
$EndComp
$Comp
L Device:L_Core_Ferrite L?
U 1 1 5FDA31D6
P 3250 6750
F 0 "L?" V 3475 6750 50  0000 C CNN
F 1 "L_Core_Ferrite" V 3384 6750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3250 6750 50  0001 C CNN
F 3 "~" H 3250 6750 50  0001 C CNN
	1    3250 6750
	0    -1   -1   0   
$EndComp
Connection ~ 2800 8500
Wire Wire Line
	3050 8500 3050 8400
Wire Wire Line
	2800 8500 3050 8500
Wire Wire Line
	2800 8500 2800 8550
Wire Wire Line
	2550 8500 2800 8500
Wire Wire Line
	2550 8400 2550 8500
Wire Wire Line
	2550 7950 3050 7950
Wire Wire Line
	3050 7950 3200 7950
Connection ~ 3050 7950
$Comp
L power:GND #PWR?
U 1 1 5FDA31E5
P 2800 8550
F 0 "#PWR?" H 2800 8300 50  0001 C CNN
F 1 "GND" V 2805 8422 50  0000 R CNN
F 2 "" H 2800 8550 50  0001 C CNN
F 3 "" H 2800 8550 50  0001 C CNN
	1    2800 8550
	1    0    0    -1  
$EndComp
Connection ~ 2550 7950
Wire Wire Line
	2550 8100 2550 7950
Wire Wire Line
	3050 8100 3050 7950
Wire Wire Line
	2350 7950 2550 7950
$Comp
L Device:CP C?
U 1 1 5FDA31F0
P 3050 8250
AR Path="/5FBA8D0D/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCC8244/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCC8817/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCC8DBD/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCC9264/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCC979C/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCC9B94/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCCA014/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FCCA439/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F63/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F69/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F6F/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F75/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F7B/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F81/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F87/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F8D/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FD39F93/5FDA31F0" Ref="C?"  Part="1" 
AR Path="/5FDA31F0" Ref="C?"  Part="1" 
F 0 "C?" H 3168 8296 50  0000 L CNN
F 1 "220u" H 3168 8205 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-30_AVX-N" H 3088 8100 50  0001 C CNN
F 3 "~" H 3050 8250 50  0001 C CNN
F 4 "F931C227MNC" H 3050 8250 50  0001 C CNN "Mouser"
	1    3050 8250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 5FDA31F7
P 2550 8250
AR Path="/5FBA8D0D/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCC8244/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCC8817/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCC8DBD/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCC9264/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCC979C/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCC9B94/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCCA014/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FCCA439/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F63/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F69/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F6F/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F75/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F7B/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F81/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F87/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F8D/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FD39F93/5FDA31F7" Ref="C?"  Part="1" 
AR Path="/5FDA31F7" Ref="C?"  Part="1" 
F 0 "C?" H 2668 8296 50  0000 L CNN
F 1 "220u" H 2668 8205 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-30_AVX-N" H 2588 8100 50  0001 C CNN
F 3 "~" H 2550 8250 50  0001 C CNN
F 4 "F931C227MNC" H 2550 8250 50  0001 C CNN "Mouser"
	1    2550 8250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 7350 2450 7350
Wire Wire Line
	1750 7350 2050 7350
$Comp
L Device:Polyfuse F?
U 1 1 5FDA31FF
P 2200 7350
F 0 "F?" V 1975 7350 50  0000 C CNN
F 1 "3A" V 2066 7350 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2250 7150 50  0001 L CNN
F 3 "~" H 2200 7350 50  0001 C CNN
	1    2200 7350
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 7450 1750 7350
Wire Wire Line
	1650 7450 1750 7450
Connection ~ 1750 7350
Wire Wire Line
	1650 7350 1750 7350
Wire Wire Line
	1750 7250 1650 7250
Wire Wire Line
	1750 7350 1750 7250
Text GLabel 2450 7350 2    50   Output ~ 0
+5V
$Comp
L Connector_Generic:Conn_01x15 J?
U 1 1 5FDA320C
P 1450 7350
F 0 "J?" H 1368 8267 50  0000 C CNN
F 1 "Power" H 1368 8176 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x15_P1.27mm_Vertical" H 1450 7350 50  0001 C CNN
F 3 "~" H 1450 7350 50  0001 C CNN
	1    1450 7350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1750 7950 2050 7950
$Comp
L Device:Polyfuse F?
U 1 1 5FDA3213
P 2200 7950
F 0 "F?" V 1975 7950 50  0000 C CNN
F 1 "5A" V 2066 7950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2250 7750 50  0001 L CNN
F 3 "~" H 2200 7950 50  0001 C CNN
	1    2200 7950
	0    1    1    0   
$EndComp
Text GLabel 3200 7950 2    50   Output ~ 0
+12V
Wire Wire Line
	1750 6750 2050 6750
$Comp
L Device:Polyfuse F?
U 1 1 5FDA321B
P 2200 6750
F 0 "F?" V 1975 6750 50  0000 C CNN
F 1 "0.5A" V 2066 6750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2250 6550 50  0001 L CNN
F 3 "~" H 2200 6750 50  0001 C CNN
	1    2200 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 7950 1750 7950
Connection ~ 1750 7950
Wire Wire Line
	1750 8050 1650 8050
Wire Wire Line
	1750 7950 1750 8050
Wire Wire Line
	1750 7850 1750 7950
Wire Wire Line
	1650 7850 1750 7850
Wire Wire Line
	1750 7550 1750 7650
Wire Wire Line
	1650 7550 1750 7550
Connection ~ 1750 7650
Wire Wire Line
	1750 7750 1650 7750
Wire Wire Line
	1750 7650 1750 7750
Wire Wire Line
	1800 7650 1750 7650
$Comp
L power:GND #PWR?
U 1 1 5FDA322D
P 1800 7650
F 0 "#PWR?" H 1800 7400 50  0001 C CNN
F 1 "GND" V 1805 7522 50  0000 R CNN
F 2 "" H 1800 7650 50  0001 C CNN
F 3 "" H 1800 7650 50  0001 C CNN
	1    1800 7650
	0    -1   -1   0   
$EndComp
Connection ~ 1750 6750
Wire Wire Line
	1650 6750 1750 6750
Wire Wire Line
	1750 6650 1650 6650
Wire Wire Line
	1750 6750 1750 6650
Text GLabel 2650 6600 1    50   Output ~ 0
+3.3V
Wire Wire Line
	1750 6950 1750 7050
Wire Wire Line
	1650 6950 1750 6950
Connection ~ 1750 7050
Wire Wire Line
	1650 7050 1750 7050
Wire Wire Line
	1750 7150 1650 7150
Wire Wire Line
	1750 7050 1750 7150
Wire Wire Line
	1800 7050 1750 7050
$Comp
L power:GND #PWR?
U 1 1 5FDA323F
P 1800 7050
F 0 "#PWR?" H 1800 6800 50  0001 C CNN
F 1 "GND" V 1805 6922 50  0000 R CNN
F 2 "" H 1800 7050 50  0001 C CNN
F 3 "" H 1800 7050 50  0001 C CNN
	1    1800 7050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3550 4150 3300 4150
Wire Wire Line
	3550 4050 3300 4050
Wire Wire Line
	3550 3950 3300 3950
Wire Wire Line
	3300 3850 3550 3850
Text GLabel 3300 3850 0    50   Input ~ 0
+3.3V
$Comp
L power:GND #PWR?
U 1 1 5FDA324A
P 3300 3950
F 0 "#PWR?" H 3300 3700 50  0001 C CNN
F 1 "GND" V 3305 3822 50  0000 R CNN
F 2 "" H 3300 3950 50  0001 C CNN
F 3 "" H 3300 3950 50  0001 C CNN
	1    3300 3950
	0    1    1    0   
$EndComp
Text Label 3300 4050 0    50   ~ 0
SCL
Text Label 3300 4150 0    50   ~ 0
SDA
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5FDA3252
P 3750 3950
F 0 "J?" H 3830 3942 50  0000 L CNN
F 1 "OLED" H 3830 3851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3750 3950 50  0001 C CNN
F 3 "~" H 3750 3950 50  0001 C CNN
	1    3750 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3450 3550 3450
Wire Wire Line
	3050 3350 3550 3350
Wire Wire Line
	3050 3250 3550 3250
Wire Wire Line
	3050 3150 3550 3150
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5FDA325C
P 3750 3250
F 0 "J?" H 3800 3567 50  0000 C CNN
F 1 "Motherboard PWM" H 3800 3476 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 3750 3250 50  0001 C CNN
F 3 "~" H 3750 3250 50  0001 C CNN
	1    3750 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5FDA3262
P 3750 2500
F 0 "J?" H 3800 2817 50  0000 C CNN
F 1 "Temp sensors" H 3800 2726 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 3750 2500 50  0001 C CNN
F 3 "~" H 3750 2500 50  0001 C CNN
	1    3750 2500
	1    0    0    -1  
$EndComp
NoConn ~ 3550 5150
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 5FDA3269
P 3750 1550
F 0 "J?" H 3830 1542 50  0000 L CNN
F 1 "LED_STRIPS" H 3830 1451 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x08_P1.27mm_Vertical" H 3750 1550 50  0001 C CNN
F 3 "~" H 3750 1550 50  0001 C CNN
	1    3750 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 6850 1750 6850
Wire Wire Line
	1750 6850 1750 6950
Connection ~ 1750 6950
Wire Wire Line
	1650 7650 1750 7650
Wire Wire Line
	4400 -500 4400 -950
$Comp
L Device:R_Pack04_Split RN?
U 4 1 5FDA3274
P 4400 -1100
AR Path="/5FBA8D0D/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCC8244/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCC8817/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCC8DBD/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCC9264/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCC979C/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCC9B94/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCCA014/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FCCA439/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F63/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F69/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F6F/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F75/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F7B/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F81/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F87/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F8D/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FD39F93/5FDA3274" Ref="RN?"  Part="1" 
AR Path="/5FDA3274" Ref="RN?"  Part="4" 
F 0 "RN?" H 4488 -1054 50  0000 L CNN
F 1 "4.7k" H 4488 -1145 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0603" V 4320 -1100 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 4400 -1100 50  0001 C CNN
	4    4400 -1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 -500 3950 -950
$Comp
L Device:R_Pack04_Split RN?
U 3 1 5FDA327B
P 3950 -1100
AR Path="/5FBA8D0D/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCC8244/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCC8817/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCC8DBD/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCC9264/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCC979C/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCC9B94/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCCA014/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FCCA439/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F63/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F69/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F6F/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F75/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F7B/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F81/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F87/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F8D/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FD39F93/5FDA327B" Ref="RN?"  Part="1" 
AR Path="/5FDA327B" Ref="RN?"  Part="3" 
F 0 "RN?" H 4038 -1054 50  0000 L CNN
F 1 "4.7k" H 4038 -1145 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0603" V 3870 -1100 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 3950 -1100 50  0001 C CNN
	3    3950 -1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 -1350 3500 -1350
Wire Wire Line
	3500 -1350 3950 -1350
Wire Wire Line
	3950 -1350 3950 -1250
Connection ~ 3500 -1350
Wire Wire Line
	3950 -1350 4400 -1350
Wire Wire Line
	4400 -1350 4400 -1250
Connection ~ 3950 -1350
Text Label 3950 -500 1    50   ~ 0
SCL2
Text Label 4400 -500 1    50   ~ 0
SDA2
$Comp
L Device:R R?
U 1 1 5FDA328A
P 3250 4550
F 0 "R?" V 3043 4550 50  0000 C CNN
F 1 "100" V 3134 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3180 4550 50  0001 C CNN
F 3 "~" H 3250 4550 50  0001 C CNN
	1    3250 4550
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5FDA3290
P 3750 4550
F 0 "J?" H 3830 4542 50  0000 L CNN
F 1 "Buzzer" H 3830 4451 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 3750 4550 50  0001 C CNN
F 3 "~" H 3750 4550 50  0001 C CNN
	1    3750 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA3296
P 3450 4750
F 0 "#PWR?" H 3450 4500 50  0001 C CNN
F 1 "GND" H 3455 4577 50  0000 C CNN
F 2 "" H 3450 4750 50  0001 C CNN
F 3 "" H 3450 4750 50  0001 C CNN
	1    3450 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4750 3450 4650
Wire Wire Line
	3450 4650 3550 4650
Wire Wire Line
	2650 6600 2650 6750
Wire Wire Line
	2350 6750 2650 6750
Connection ~ 2950 6750
Connection ~ 2650 6750
Wire Wire Line
	2650 6750 2950 6750
Wire Wire Line
	2800 4550 3100 4550
Wire Wire Line
	3400 4550 3550 4550
Text Label 1450 2900 0    50   ~ 0
PWM_1
Text Label 1450 3000 0    50   ~ 0
PWM_2
Text Label 1450 3100 0    50   ~ 0
PWM_3
Text Label 1450 3200 0    50   ~ 0
PWM_4
Text Label 1450 3300 0    50   ~ 0
PWM_5
Text Label 1450 3400 0    50   ~ 0
PWM_6
Text Label 1450 3500 0    50   ~ 0
PWM_7
Text Label 1450 3600 0    50   ~ 0
PWM_8
Text Label 1450 3700 0    50   ~ 0
PWM_9
Text Label 1450 3800 0    50   ~ 0
PWM_10
Text Label 1450 3900 0    50   ~ 0
PWM_11
Text Label 1450 4000 0    50   ~ 0
PWM_12
Text Label 1450 4100 0    50   ~ 0
PWM_13
Text Label 1450 4200 0    50   ~ 0
PWM_14
Text Label 1450 4300 0    50   ~ 0
PWM_15
Text Label 1450 4400 0    50   ~ 0
PWM_16
Wire Wire Line
	2000 2900 1450 2900
Wire Wire Line
	2000 3000 1450 3000
Wire Wire Line
	2000 3100 1450 3100
Wire Wire Line
	2000 3200 1450 3200
Wire Wire Line
	2000 3300 1450 3300
Wire Wire Line
	2000 3400 1450 3400
Wire Wire Line
	2000 3500 1450 3500
Wire Wire Line
	2000 3600 1450 3600
Wire Wire Line
	2000 3700 1450 3700
Wire Wire Line
	2000 3800 1450 3800
Wire Wire Line
	2000 3900 1450 3900
Wire Wire Line
	2000 4000 1450 4000
Wire Wire Line
	2000 4100 1450 4100
Wire Wire Line
	2000 4200 1450 4200
Wire Wire Line
	2000 4300 1450 4300
Wire Wire Line
	2000 4400 1450 4400
Text Label 2800 4550 0    50   ~ 0
BUZZER
Text GLabel 2950 -1350 0    50   Input ~ 0
+3.3V
Wire Wire Line
	2950 -1350 3050 -1350
Connection ~ 3050 -1350
Text Label 1450 750  0    50   ~ 0
PWR_FAN_1
Text Label 1450 850  0    50   ~ 0
PWR_FAN_2
Text Label 1450 950  0    50   ~ 0
PWR_FAN_3
Text Label 1450 1050 0    50   ~ 0
PWR_FAN_4
Text Label 1450 1150 0    50   ~ 0
PWR_FAN_5
Text Label 1450 1250 0    50   ~ 0
PWR_FAN_6
Text Label 1450 1350 0    50   ~ 0
PWR_FAN_7
Text Label 1450 1450 0    50   ~ 0
PWR_FAN_8
Text Label 1450 1550 0    50   ~ 0
PWR_FAN_9
Text Label 1450 1650 0    50   ~ 0
PWR_FAN_10
Text Label 1450 1750 0    50   ~ 0
PWR_FAN_11
Text Label 1450 1850 0    50   ~ 0
PWR_FAN_12
Text Label 1450 1950 0    50   ~ 0
PWR_FAN_13
Text Label 1450 2050 0    50   ~ 0
PWR_FAN_14
Text Label 1450 2150 0    50   ~ 0
PWR_FAN_15
Text Label 1450 2250 0    50   ~ 0
PWR_FAN_16
Wire Wire Line
	1450 750  2000 750 
Wire Wire Line
	1450 850  2000 850 
Wire Wire Line
	1450 950  2000 950 
Wire Wire Line
	1450 1050 2000 1050
Wire Wire Line
	1450 1150 2000 1150
Wire Wire Line
	1450 1250 2000 1250
Wire Wire Line
	1450 1350 2000 1350
Wire Wire Line
	1450 1450 2000 1450
Wire Wire Line
	1450 1550 2000 1550
Wire Wire Line
	1450 1650 2000 1650
Wire Wire Line
	1450 1750 2000 1750
Wire Wire Line
	1450 1850 2000 1850
Wire Wire Line
	1450 1950 2000 1950
Wire Wire Line
	1450 2050 2000 2050
Wire Wire Line
	1450 2150 2000 2150
Wire Wire Line
	1450 2250 2000 2250
Text Label 3050 2400 0    50   ~ 0
TEMP_1
Text Label 3050 2500 0    50   ~ 0
TEMP_2
Text Label 3050 2600 0    50   ~ 0
TEMP_3
Text Label 3050 2700 0    50   ~ 0
TEMP_4
Wire Wire Line
	3050 2400 3550 2400
Wire Wire Line
	3050 2500 3550 2500
Wire Wire Line
	3050 2600 3550 2600
Wire Wire Line
	3050 2700 3550 2700
$Comp
L Connector_Generic:Conn_01x16 J?
U 1 1 5FDA32F1
P 2200 3600
F 0 "J?" H 2280 3592 50  0000 L CNN
F 1 "Conn_01x16" H 2280 3501 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x16_P1.27mm_Vertical" H 2200 3600 50  0001 C CNN
F 3 "~" H 2200 3600 50  0001 C CNN
	1    2200 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x16 J?
U 1 1 5FDA32F7
P 2200 5250
F 0 "J?" H 2280 5242 50  0000 L CNN
F 1 "Conn_01x16" H 2280 5151 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x16_P1.27mm_Vertical" H 2200 5250 50  0001 C CNN
F 3 "~" H 2200 5250 50  0001 C CNN
	1    2200 5250
	1    0    0    -1  
$EndComp
Text Label 3050 3150 0    50   ~ 0
MB_PWM_1
Text Label 3050 3250 0    50   ~ 0
MB_PWM_2
Text Label 3050 3350 0    50   ~ 0
MB_PWM_3
Text Label 3050 3450 0    50   ~ 0
MB_PWM_4
Text Label 3000 1250 0    50   ~ 0
LEDSTRIP_0
Wire Wire Line
	3550 1350 3000 1350
Wire Wire Line
	3550 1450 3000 1450
Wire Wire Line
	3550 1550 3000 1550
Text Label 3000 1350 0    50   ~ 0
LEDSTRIP_1
Text Label 3000 1450 0    50   ~ 0
LEDSTRIP_2
Text Label 3000 1550 0    50   ~ 0
LEDSTRIP_3
Text Label 3000 1650 0    50   ~ 0
LEDSTRIP_4
Text Label 3000 1750 0    50   ~ 0
LEDSTRIP_5
Text Label 3000 1850 0    50   ~ 0
LEDSTRIP_6
Text Label 3000 1950 0    50   ~ 0
LEDSTRIP_7
Wire Wire Line
	3000 1250 3550 1250
Wire Wire Line
	3550 1650 3000 1650
Wire Wire Line
	3550 1750 3000 1750
Wire Wire Line
	3550 1850 3000 1850
Wire Wire Line
	3550 1950 3000 1950
Text Notes 1600 200  0    118  ~ 0
To connector board
Text Label 1450 4550 0    50   ~ 0
TACH_1
Text Label 1450 4650 0    50   ~ 0
TACH_2
Text Label 1450 4750 0    50   ~ 0
TACH_3
Text Label 1450 4850 0    50   ~ 0
TACH_4
Text Label 1450 4950 0    50   ~ 0
TACH_5
Text Label 1450 5050 0    50   ~ 0
TACH_6
Text Label 1450 5150 0    50   ~ 0
TACH_7
Text Label 1450 5250 0    50   ~ 0
TACH_8
Text Label 1450 5350 0    50   ~ 0
TACH_9
Text Label 1450 5450 0    50   ~ 0
TACH_10
Text Label 1450 5550 0    50   ~ 0
TACH_11
Text Label 1450 5650 0    50   ~ 0
TACH_12
Text Label 1450 5750 0    50   ~ 0
TACH_13
Text Label 1450 5850 0    50   ~ 0
TACH_14
Text Label 1450 5950 0    50   ~ 0
TACH_15
Text Label 1450 6050 0    50   ~ 0
TACH_16
Wire Wire Line
	2000 4550 1450 4550
Wire Wire Line
	2000 4650 1450 4650
Wire Wire Line
	2000 4750 1450 4750
Wire Wire Line
	2000 4850 1450 4850
Wire Wire Line
	2000 4950 1450 4950
Wire Wire Line
	2000 5050 1450 5050
Wire Wire Line
	2000 5150 1450 5150
Wire Wire Line
	2000 5250 1450 5250
Wire Wire Line
	2000 5350 1450 5350
Wire Wire Line
	2000 5450 1450 5450
Wire Wire Line
	2000 5550 1450 5550
Wire Wire Line
	2000 5650 1450 5650
Wire Wire Line
	2000 5750 1450 5750
Wire Wire Line
	2000 5850 1450 5850
Wire Wire Line
	2000 5950 1450 5950
Wire Wire Line
	2000 6050 1450 6050
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5FDA3332
P 900 -400
F 0 "H?" H 1000 -351 50  0000 L CNN
F 1 "MountingHole_Pad" H 1000 -442 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 900 -400 50  0001 C CNN
F 3 "~" H 900 -400 50  0001 C CNN
	1    900  -400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA3338
P 900 -300
F 0 "#PWR?" H 900 -550 50  0001 C CNN
F 1 "GND" H 905 -473 50  0000 C CNN
F 2 "" H 900 -300 50  0001 C CNN
F 3 "" H 900 -300 50  0001 C CNN
	1    900  -300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5FDA333E
P 1200 -400
F 0 "H?" H 1300 -351 50  0000 L CNN
F 1 "MountingHole_Pad" H 1300 -442 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1200 -400 50  0001 C CNN
F 3 "~" H 1200 -400 50  0001 C CNN
	1    1200 -400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA3344
P 1200 -300
F 0 "#PWR?" H 1200 -550 50  0001 C CNN
F 1 "GND" H 1205 -473 50  0000 C CNN
F 2 "" H 1200 -300 50  0001 C CNN
F 3 "" H 1200 -300 50  0001 C CNN
	1    1200 -300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5FDA334A
P 1450 -400
F 0 "H?" H 1550 -351 50  0000 L CNN
F 1 "MountingHole_Pad" H 1550 -442 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1450 -400 50  0001 C CNN
F 3 "~" H 1450 -400 50  0001 C CNN
	1    1450 -400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA3350
P 1450 -300
F 0 "#PWR?" H 1450 -550 50  0001 C CNN
F 1 "GND" H 1455 -473 50  0000 C CNN
F 2 "" H 1450 -300 50  0001 C CNN
F 3 "" H 1450 -300 50  0001 C CNN
	1    1450 -300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5FDA3356
P 1700 -400
F 0 "H?" H 1800 -351 50  0000 L CNN
F 1 "MountingHole_Pad" H 1800 -442 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1700 -400 50  0001 C CNN
F 3 "~" H 1700 -400 50  0001 C CNN
	1    1700 -400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA335C
P 1700 -300
F 0 "#PWR?" H 1700 -550 50  0001 C CNN
F 1 "GND" H 1705 -473 50  0000 C CNN
F 2 "" H 1700 -300 50  0001 C CNN
F 3 "" H 1700 -300 50  0001 C CNN
	1    1700 -300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x24 J?
U 1 1 5FDA3362
P 2200 1450
F 0 "J?" H 2280 1442 50  0000 L CNN
F 1 "Conn_01x24" H 2280 1351 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x24_P1.27mm_Vertical" H 2200 1450 50  0001 C CNN
F 3 "~" H 2200 1450 50  0001 C CNN
	1    2200 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA3368
P 1700 2500
F 0 "#PWR?" H 1700 2250 50  0001 C CNN
F 1 "GND" V 1705 2372 50  0000 R CNN
F 2 "" H 1700 2500 50  0001 C CNN
F 3 "" H 1700 2500 50  0001 C CNN
	1    1700 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 2500 1850 2500
Wire Wire Line
	1850 2500 1850 2450
Wire Wire Line
	1850 2350 2000 2350
Wire Wire Line
	2000 2450 1850 2450
Connection ~ 1850 2450
Wire Wire Line
	1850 2450 1850 2350
Wire Wire Line
	1850 2500 1850 2550
Wire Wire Line
	1850 2650 2000 2650
Connection ~ 1850 2500
Wire Wire Line
	2000 2550 1850 2550
Connection ~ 1850 2550
Wire Wire Line
	1850 2550 1850 2650
$Comp
L power:GND #PWR?
U 1 1 5FDA337A
P 1700 500
F 0 "#PWR?" H 1700 250 50  0001 C CNN
F 1 "GND" V 1705 372 50  0000 R CNN
F 2 "" H 1700 500 50  0001 C CNN
F 3 "" H 1700 500 50  0001 C CNN
	1    1700 500 
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 500  1850 500 
Wire Wire Line
	1850 500  1850 450 
Wire Wire Line
	1850 350  2000 350 
Wire Wire Line
	2000 450  1850 450 
Connection ~ 1850 450 
Wire Wire Line
	1850 450  1850 350 
Wire Wire Line
	1850 500  1850 550 
Wire Wire Line
	1850 650  2000 650 
Connection ~ 1850 500 
Wire Wire Line
	2000 550  1850 550 
Connection ~ 1850 550 
Wire Wire Line
	1850 550  1850 650 
$EndSCHEMATC
