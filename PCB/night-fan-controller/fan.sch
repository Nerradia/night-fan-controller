EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1800 1000 0    50   Input ~ 0
+12V
$Comp
L Device:L L201
U 1 1 5FBB0C6D
P 4850 2050
AR Path="/5FBA8D0D/5FBB0C6D" Ref="L201"  Part="1" 
AR Path="/5FCC8244/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FCC8817/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FCC8DBD/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FCC9264/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FCC979C/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FCC9B94/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FCCA014/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FCCA439/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F63/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F69/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F6F/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F75/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F7B/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F81/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F87/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F8D/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/5FD39F93/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/619B6E22/5FBB0C6D" Ref="L301"  Part="1" 
AR Path="/61CC18AC/5FBB0C6D" Ref="L401"  Part="1" 
AR Path="/61D0F570/5FBB0C6D" Ref="L501"  Part="1" 
AR Path="/61D5D81A/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/61DAB55E/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/61DF933A/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/61E47116/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/61E94EE2/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/61EE2CE6/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E396/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E39F/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E3A8/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E3B1/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E3BA/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E3C3/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E3CC/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/6206E3D5/5FBB0C6D" Ref="L?"  Part="1" 
AR Path="/600B24BA/5FBB0C6D" Ref="L601"  Part="1" 
AR Path="/600B24BF/5FBB0C6D" Ref="L701"  Part="1" 
AR Path="/600B24C4/5FBB0C6D" Ref="L801"  Part="1" 
AR Path="/600B24C9/5FBB0C6D" Ref="L901"  Part="1" 
AR Path="/60734BB2/5FBB0C6D" Ref="L1001"  Part="1" 
AR Path="/60734BB7/5FBB0C6D" Ref="L1101"  Part="1" 
AR Path="/60734BBC/5FBB0C6D" Ref="L1201"  Part="1" 
AR Path="/60734BC1/5FBB0C6D" Ref="L1301"  Part="1" 
AR Path="/60734BC6/5FBB0C6D" Ref="L1401"  Part="1" 
AR Path="/60734BCB/5FBB0C6D" Ref="L1501"  Part="1" 
AR Path="/60734BD0/5FBB0C6D" Ref="L1601"  Part="1" 
AR Path="/60734BD5/5FBB0C6D" Ref="L1701"  Part="1" 
F 0 "L201" V 5040 2050 50  0000 C CNN
F 1 "10u" V 4949 2050 50  0000 C CNN
F 2 "Inductor_SMD:L_Bourns-SRN6028" H 4850 2050 50  0001 C CNN
F 3 "~" H 4850 2050 50  0001 C CNN
F 4 "875-TYS6045100M-10" H 4850 2050 50  0001 C CNN "Mouser"
	1    4850 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C202
U 1 1 5FBB21F7
P 5200 2400
AR Path="/5FBA8D0D/5FBB21F7" Ref="C202"  Part="1" 
AR Path="/5FCC8244/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FCC8817/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FCC8DBD/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FCC9264/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FCC979C/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FCC9B94/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FCCA014/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FCCA439/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F63/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F69/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F6F/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F75/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F7B/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F81/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F87/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F8D/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/5FD39F93/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/619B6E22/5FBB21F7" Ref="C302"  Part="1" 
AR Path="/61CC18AC/5FBB21F7" Ref="C402"  Part="1" 
AR Path="/61D0F570/5FBB21F7" Ref="C502"  Part="1" 
AR Path="/61D5D81A/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/61DAB55E/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/61DF933A/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/61E47116/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/61E94EE2/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/61EE2CE6/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E396/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E39F/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E3A8/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E3B1/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E3BA/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E3C3/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E3CC/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/6206E3D5/5FBB21F7" Ref="C?"  Part="1" 
AR Path="/600B24BA/5FBB21F7" Ref="C602"  Part="1" 
AR Path="/600B24BF/5FBB21F7" Ref="C702"  Part="1" 
AR Path="/600B24C4/5FBB21F7" Ref="C802"  Part="1" 
AR Path="/600B24C9/5FBB21F7" Ref="C902"  Part="1" 
AR Path="/60734BB2/5FBB21F7" Ref="C1002"  Part="1" 
AR Path="/60734BB7/5FBB21F7" Ref="C1102"  Part="1" 
AR Path="/60734BBC/5FBB21F7" Ref="C1202"  Part="1" 
AR Path="/60734BC1/5FBB21F7" Ref="C1302"  Part="1" 
AR Path="/60734BC6/5FBB21F7" Ref="C1402"  Part="1" 
AR Path="/60734BCB/5FBB21F7" Ref="C1502"  Part="1" 
AR Path="/60734BD0/5FBB21F7" Ref="C1602"  Part="1" 
AR Path="/60734BD5/5FBB21F7" Ref="C1702"  Part="1" 
F 0 "C202" H 5318 2446 50  0000 L CNN
F 1 "220u" H 5318 2355 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-30_AVX-N" H 5238 2250 50  0001 C CNN
F 3 "~" H 5200 2400 50  0001 C CNN
F 4 "647-F931C227MNC" H 5200 2400 50  0001 C CNN "Mouser"
	1    5200 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C203
U 1 1 5FBB2C70
P 5550 2400
AR Path="/5FBA8D0D/5FBB2C70" Ref="C203"  Part="1" 
AR Path="/5FCC8244/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FCC8817/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FCC8DBD/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FCC9264/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FCC979C/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FCC9B94/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FCCA014/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FCCA439/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F63/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F69/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F6F/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F75/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F7B/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F81/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F87/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F8D/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/5FD39F93/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/619B6E22/5FBB2C70" Ref="C303"  Part="1" 
AR Path="/61CC18AC/5FBB2C70" Ref="C403"  Part="1" 
AR Path="/61D0F570/5FBB2C70" Ref="C503"  Part="1" 
AR Path="/61D5D81A/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/61DAB55E/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/61DF933A/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/61E47116/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/61E94EE2/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/61EE2CE6/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E396/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E39F/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E3A8/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E3B1/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E3BA/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E3C3/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E3CC/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/6206E3D5/5FBB2C70" Ref="C?"  Part="1" 
AR Path="/600B24BA/5FBB2C70" Ref="C603"  Part="1" 
AR Path="/600B24BF/5FBB2C70" Ref="C703"  Part="1" 
AR Path="/600B24C4/5FBB2C70" Ref="C803"  Part="1" 
AR Path="/600B24C9/5FBB2C70" Ref="C903"  Part="1" 
AR Path="/60734BB2/5FBB2C70" Ref="C1003"  Part="1" 
AR Path="/60734BB7/5FBB2C70" Ref="C1103"  Part="1" 
AR Path="/60734BBC/5FBB2C70" Ref="C1203"  Part="1" 
AR Path="/60734BC1/5FBB2C70" Ref="C1303"  Part="1" 
AR Path="/60734BC6/5FBB2C70" Ref="C1403"  Part="1" 
AR Path="/60734BCB/5FBB2C70" Ref="C1503"  Part="1" 
AR Path="/60734BD0/5FBB2C70" Ref="C1603"  Part="1" 
AR Path="/60734BD5/5FBB2C70" Ref="C1703"  Part="1" 
F 0 "C203" H 5665 2446 50  0000 L CNN
F 1 "220n" H 5665 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5588 2250 50  0001 C CNN
F 3 "~" H 5550 2400 50  0001 C CNN
F 4 "187-CL10B224KO8NNNC" H 5550 2400 50  0001 C CNN "Mouser"
	1    5550 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2050 5200 2050
Wire Wire Line
	5550 2250 5550 2050
Wire Wire Line
	5200 2250 5200 2050
Connection ~ 5200 2050
Wire Wire Line
	5200 2050 5550 2050
$Comp
L lm5106:LM5106 U201
U 1 1 5FBB42E0
P 3000 2000
AR Path="/5FBA8D0D/5FBB42E0" Ref="U201"  Part="1" 
AR Path="/5FCC8244/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FCC8817/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FCC8DBD/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FCC9264/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FCC979C/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FCC9B94/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FCCA014/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FCCA439/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F63/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F69/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F6F/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F75/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F7B/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F81/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F87/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F8D/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/5FD39F93/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/619B6E22/5FBB42E0" Ref="U301"  Part="1" 
AR Path="/61CC18AC/5FBB42E0" Ref="U401"  Part="1" 
AR Path="/61D0F570/5FBB42E0" Ref="U501"  Part="1" 
AR Path="/61D5D81A/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/61DAB55E/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/61DF933A/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/61E47116/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/61E94EE2/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/61EE2CE6/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E396/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E39F/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E3A8/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E3B1/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E3BA/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E3C3/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E3CC/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/6206E3D5/5FBB42E0" Ref="U?"  Part="1" 
AR Path="/600B24BA/5FBB42E0" Ref="U601"  Part="1" 
AR Path="/600B24BF/5FBB42E0" Ref="U701"  Part="1" 
AR Path="/600B24C4/5FBB42E0" Ref="U801"  Part="1" 
AR Path="/600B24C9/5FBB42E0" Ref="U901"  Part="1" 
AR Path="/60734BB2/5FBB42E0" Ref="U1001"  Part="1" 
AR Path="/60734BB7/5FBB42E0" Ref="U1101"  Part="1" 
AR Path="/60734BBC/5FBB42E0" Ref="U1201"  Part="1" 
AR Path="/60734BC1/5FBB42E0" Ref="U1301"  Part="1" 
AR Path="/60734BC6/5FBB42E0" Ref="U1401"  Part="1" 
AR Path="/60734BCB/5FBB42E0" Ref="U1501"  Part="1" 
AR Path="/60734BD0/5FBB42E0" Ref="U1601"  Part="1" 
AR Path="/60734BD5/5FBB42E0" Ref="U1701"  Part="1" 
F 0 "U201" H 3200 2450 60  0000 C CNN
F 1 "LM5106" H 3250 2350 60  0000 C CNN
F 2 "lib:SON80P400X400X80-11N" H 3050 2000 60  0001 C CNN
F 3 "" H 3050 2000 60  0001 C CNN
F 4 "926-LM5106SD/NOPB" H 3000 2000 50  0001 C CNN "Mouser"
	1    3000 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3100 5200 2550
Wire Wire Line
	5550 3100 5550 2550
$Comp
L Device:D D201
U 1 1 5FBCCA08
P 2700 1300
AR Path="/5FBA8D0D/5FBCCA08" Ref="D201"  Part="1" 
AR Path="/5FCC8244/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FCC8817/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FCC8DBD/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FCC9264/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FCC979C/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FCC9B94/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FCCA014/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FCCA439/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F63/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F69/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F6F/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F75/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F7B/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F81/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F87/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F8D/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/5FD39F93/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/619B6E22/5FBCCA08" Ref="D301"  Part="1" 
AR Path="/61CC18AC/5FBCCA08" Ref="D401"  Part="1" 
AR Path="/61D0F570/5FBCCA08" Ref="D501"  Part="1" 
AR Path="/61D5D81A/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/61DAB55E/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/61DF933A/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/61E47116/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/61E94EE2/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/61EE2CE6/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E396/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E39F/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E3A8/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E3B1/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E3BA/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E3C3/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E3CC/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/6206E3D5/5FBCCA08" Ref="D?"  Part="1" 
AR Path="/600B24BA/5FBCCA08" Ref="D601"  Part="1" 
AR Path="/600B24BF/5FBCCA08" Ref="D701"  Part="1" 
AR Path="/600B24C4/5FBCCA08" Ref="D801"  Part="1" 
AR Path="/600B24C9/5FBCCA08" Ref="D901"  Part="1" 
AR Path="/60734BB2/5FBCCA08" Ref="D1001"  Part="1" 
AR Path="/60734BB7/5FBCCA08" Ref="D1101"  Part="1" 
AR Path="/60734BBC/5FBCCA08" Ref="D1201"  Part="1" 
AR Path="/60734BC1/5FBCCA08" Ref="D1301"  Part="1" 
AR Path="/60734BC6/5FBCCA08" Ref="D1401"  Part="1" 
AR Path="/60734BCB/5FBCCA08" Ref="D1501"  Part="1" 
AR Path="/60734BD0/5FBCCA08" Ref="D1601"  Part="1" 
AR Path="/60734BD5/5FBCCA08" Ref="D1701"  Part="1" 
F 0 "D201" H 2700 1083 50  0000 C CNN
F 1 "CDBU0340-HF" H 2700 1174 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric" H 2700 1300 50  0001 C CNN
F 3 "~" H 2700 1300 50  0001 C CNN
F 4 "750-CDBU0340-HF" H 2700 1300 50  0001 C CNN "Mouser"
	1    2700 1300
	-1   0    0    1   
$EndComp
$Comp
L Device:C C201
U 1 1 5FBCD749
P 3800 1600
AR Path="/5FBA8D0D/5FBCD749" Ref="C201"  Part="1" 
AR Path="/5FCC8244/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FCC8817/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FCC8DBD/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FCC9264/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FCC979C/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FCC9B94/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FCCA014/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FCCA439/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F63/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F69/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F6F/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F75/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F7B/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F81/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F87/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F8D/5FBCD749" Ref="C?"  Part="1" 
AR Path="/5FD39F93/5FBCD749" Ref="C?"  Part="1" 
AR Path="/619B6E22/5FBCD749" Ref="C301"  Part="1" 
AR Path="/61CC18AC/5FBCD749" Ref="C401"  Part="1" 
AR Path="/61D0F570/5FBCD749" Ref="C501"  Part="1" 
AR Path="/61D5D81A/5FBCD749" Ref="C?"  Part="1" 
AR Path="/61DAB55E/5FBCD749" Ref="C?"  Part="1" 
AR Path="/61DF933A/5FBCD749" Ref="C?"  Part="1" 
AR Path="/61E47116/5FBCD749" Ref="C?"  Part="1" 
AR Path="/61E94EE2/5FBCD749" Ref="C?"  Part="1" 
AR Path="/61EE2CE6/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E396/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E39F/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E3A8/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E3B1/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E3BA/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E3C3/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E3CC/5FBCD749" Ref="C?"  Part="1" 
AR Path="/6206E3D5/5FBCD749" Ref="C?"  Part="1" 
AR Path="/600B24BA/5FBCD749" Ref="C601"  Part="1" 
AR Path="/600B24BF/5FBCD749" Ref="C701"  Part="1" 
AR Path="/600B24C4/5FBCD749" Ref="C801"  Part="1" 
AR Path="/600B24C9/5FBCD749" Ref="C901"  Part="1" 
AR Path="/60734BB2/5FBCD749" Ref="C1001"  Part="1" 
AR Path="/60734BB7/5FBCD749" Ref="C1101"  Part="1" 
AR Path="/60734BBC/5FBCD749" Ref="C1201"  Part="1" 
AR Path="/60734BC1/5FBCD749" Ref="C1301"  Part="1" 
AR Path="/60734BC6/5FBCD749" Ref="C1401"  Part="1" 
AR Path="/60734BCB/5FBCD749" Ref="C1501"  Part="1" 
AR Path="/60734BD0/5FBCD749" Ref="C1601"  Part="1" 
AR Path="/60734BD5/5FBCD749" Ref="C1701"  Part="1" 
F 0 "C201" H 3915 1646 50  0000 L CNN
F 1 "220n" H 3915 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3838 1450 50  0001 C CNN
F 3 "~" H 3800 1600 50  0001 C CNN
F 4 "187-CL10B224KO8NNNC" H 3800 1600 50  0001 C CNN "Mouser"
	1    3800 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1750 3800 2050
Wire Wire Line
	3800 2050 3650 2050
Wire Wire Line
	3650 2200 4150 2200
Wire Wire Line
	3800 1450 3800 1300
Wire Wire Line
	3000 1300 3000 1500
Wire Wire Line
	3000 1300 3800 1300
Wire Wire Line
	2850 1300 3000 1300
Connection ~ 3000 1300
Wire Wire Line
	1800 1000 2000 1000
Connection ~ 3000 3100
$Comp
L Device:C C204
U 1 1 5FBDBC35
P 2000 2650
AR Path="/5FBA8D0D/5FBDBC35" Ref="C204"  Part="1" 
AR Path="/5FCC8244/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FCC8817/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FCC8DBD/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FCC9264/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FCC979C/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FCC9B94/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FCCA014/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FCCA439/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F63/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F69/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F6F/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F75/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F7B/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F81/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F87/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F8D/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/5FD39F93/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/619B6E22/5FBDBC35" Ref="C304"  Part="1" 
AR Path="/61CC18AC/5FBDBC35" Ref="C404"  Part="1" 
AR Path="/61D0F570/5FBDBC35" Ref="C504"  Part="1" 
AR Path="/61D5D81A/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/61DAB55E/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/61DF933A/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/61E47116/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/61E94EE2/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/61EE2CE6/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E396/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E39F/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E3A8/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E3B1/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E3BA/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E3C3/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E3CC/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/6206E3D5/5FBDBC35" Ref="C?"  Part="1" 
AR Path="/600B24BA/5FBDBC35" Ref="C604"  Part="1" 
AR Path="/600B24BF/5FBDBC35" Ref="C704"  Part="1" 
AR Path="/600B24C4/5FBDBC35" Ref="C804"  Part="1" 
AR Path="/600B24C9/5FBDBC35" Ref="C904"  Part="1" 
AR Path="/60734BB2/5FBDBC35" Ref="C1004"  Part="1" 
AR Path="/60734BB7/5FBDBC35" Ref="C1104"  Part="1" 
AR Path="/60734BBC/5FBDBC35" Ref="C1204"  Part="1" 
AR Path="/60734BC1/5FBDBC35" Ref="C1304"  Part="1" 
AR Path="/60734BC6/5FBDBC35" Ref="C1404"  Part="1" 
AR Path="/60734BCB/5FBDBC35" Ref="C1504"  Part="1" 
AR Path="/60734BD0/5FBDBC35" Ref="C1604"  Part="1" 
AR Path="/60734BD5/5FBDBC35" Ref="C1704"  Part="1" 
F 0 "C204" H 2115 2696 50  0000 L CNN
F 1 "220n" H 2115 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2038 2500 50  0001 C CNN
F 3 "~" H 2000 2650 50  0001 C CNN
F 4 "187-CL10B224KO8NNNC" H 2000 2650 50  0001 C CNN "Mouser"
	1    2000 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2200 2350 2200
Wire Wire Line
	2000 1000 2000 1300
Connection ~ 2000 1000
Wire Wire Line
	2000 1900 2350 1900
Wire Wire Line
	2000 1000 4450 1000
Wire Wire Line
	2000 1300 2550 1300
Connection ~ 2000 1300
Wire Wire Line
	2000 1300 2000 1900
Wire Wire Line
	2000 2500 2000 1900
Connection ~ 2000 1900
Wire Wire Line
	2000 2800 2000 3100
Text HLabel 1400 2100 0    50   Input ~ 0
PWR_EN
Text HLabel 1400 2000 0    50   Input ~ 0
PWR_PWM
Wire Wire Line
	5850 2050 5550 2050
Connection ~ 5550 2050
$Comp
L power:GND #PWR0201
U 1 1 5FC375AA
P 2000 3200
AR Path="/5FBA8D0D/5FC375AA" Ref="#PWR0201"  Part="1" 
AR Path="/5FCC8244/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FCC8817/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FCC8DBD/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FCC9264/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FCC979C/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FCC9B94/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA014/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA439/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F63/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F69/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F6F/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F75/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F7B/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F81/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F87/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F8D/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/5FD39F93/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/619B6E22/5FC375AA" Ref="#PWR0301"  Part="1" 
AR Path="/61CC18AC/5FC375AA" Ref="#PWR0401"  Part="1" 
AR Path="/61D0F570/5FC375AA" Ref="#PWR0501"  Part="1" 
AR Path="/61D5D81A/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/61DAB55E/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/61DF933A/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/61E47116/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/61E94EE2/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/61EE2CE6/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E396/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E39F/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E3A8/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E3B1/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E3BA/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E3C3/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E3CC/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/6206E3D5/5FC375AA" Ref="#PWR?"  Part="1" 
AR Path="/600B24BA/5FC375AA" Ref="#PWR0601"  Part="1" 
AR Path="/600B24BF/5FC375AA" Ref="#PWR0701"  Part="1" 
AR Path="/600B24C4/5FC375AA" Ref="#PWR0801"  Part="1" 
AR Path="/600B24C9/5FC375AA" Ref="#PWR0901"  Part="1" 
AR Path="/60734BB2/5FC375AA" Ref="#PWR01001"  Part="1" 
AR Path="/60734BB7/5FC375AA" Ref="#PWR01101"  Part="1" 
AR Path="/60734BBC/5FC375AA" Ref="#PWR01201"  Part="1" 
AR Path="/60734BC1/5FC375AA" Ref="#PWR01301"  Part="1" 
AR Path="/60734BC6/5FC375AA" Ref="#PWR01401"  Part="1" 
AR Path="/60734BCB/5FC375AA" Ref="#PWR01501"  Part="1" 
AR Path="/60734BD0/5FC375AA" Ref="#PWR01601"  Part="1" 
AR Path="/60734BD5/5FC375AA" Ref="#PWR01701"  Part="1" 
F 0 "#PWR01701" H 2000 2950 50  0001 C CNN
F 1 "GND" H 2005 3027 50  0000 C CNN
F 2 "" H 2000 3200 50  0001 C CNN
F 3 "" H 2000 3200 50  0001 C CNN
	1    2000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3200 2000 3100
Connection ~ 2000 3100
$Comp
L DMT3020LDV-7:DMT3020LDV-7 Q201
U 1 1 60C08FEB
P 4350 1800
AR Path="/5FBA8D0D/60C08FEB" Ref="Q201"  Part="1" 
AR Path="/5FCC8244/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FCC8817/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FCC8DBD/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FCC9264/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FCC979C/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FCC9B94/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FCCA014/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FCCA439/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F63/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F69/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F6F/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F75/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F7B/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F81/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F87/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F8D/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/5FD39F93/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/619B6E22/60C08FEB" Ref="Q301"  Part="1" 
AR Path="/61CC18AC/60C08FEB" Ref="Q401"  Part="1" 
AR Path="/61D0F570/60C08FEB" Ref="Q501"  Part="1" 
AR Path="/61D5D81A/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/61DAB55E/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/61DF933A/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/61E47116/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/61E94EE2/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/61EE2CE6/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E396/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E39F/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E3A8/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E3B1/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E3BA/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E3C3/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E3CC/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/6206E3D5/60C08FEB" Ref="Q?"  Part="1" 
AR Path="/600B24BA/60C08FEB" Ref="Q601"  Part="1" 
AR Path="/600B24BF/60C08FEB" Ref="Q701"  Part="1" 
AR Path="/600B24C4/60C08FEB" Ref="Q801"  Part="1" 
AR Path="/600B24C9/60C08FEB" Ref="Q901"  Part="1" 
AR Path="/60734BB2/60C08FEB" Ref="Q1001"  Part="1" 
AR Path="/60734BB7/60C08FEB" Ref="Q1101"  Part="1" 
AR Path="/60734BBC/60C08FEB" Ref="Q1201"  Part="1" 
AR Path="/60734BC1/60C08FEB" Ref="Q1301"  Part="1" 
AR Path="/60734BC6/60C08FEB" Ref="Q1401"  Part="1" 
AR Path="/60734BCB/60C08FEB" Ref="Q1501"  Part="1" 
AR Path="/60734BD0/60C08FEB" Ref="Q1601"  Part="1" 
AR Path="/60734BD5/60C08FEB" Ref="Q1701"  Part="1" 
F 0 "Q201" H 4558 1846 50  0000 L CNN
F 1 "DMT3020LDV-7" H 4550 2000 50  0000 L CNN
F 2 "lib:TRANS_DMT3020LDV-7" H 4350 1800 50  0001 L BNN
F 3 "" H 4350 1800 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 4350 1800 50  0001 L BNN "STANDARD"
F 5 "2-2" H 4350 1800 50  0001 L BNN "PARTREV"
F 6 "DIODES Incorporated" H 4350 1800 50  0001 L BNN "MANUFACTURER"
F 7 "0.85 mm" H 4350 1800 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 8 "621-DMT3020LDV-7" H 4350 1800 50  0001 C CNN "Mouser"
	1    4350 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1900 4250 1900
Wire Wire Line
	4450 1600 4450 1000
Wire Wire Line
	3000 3100 4450 3100
$Comp
L DMT3020LDV-7:DMT3020LDV-7 Q201
U 2 1 60C151A0
P 4350 2300
AR Path="/5FBA8D0D/60C151A0" Ref="Q201"  Part="2" 
AR Path="/5FCC8244/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FCC8817/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FCC8DBD/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FCC9264/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FCC979C/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FCC9B94/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FCCA014/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FCCA439/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F63/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F69/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F6F/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F75/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F7B/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F81/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F87/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F8D/60C151A0" Ref="Q?"  Part="2" 
AR Path="/5FD39F93/60C151A0" Ref="Q?"  Part="2" 
AR Path="/619B6E22/60C151A0" Ref="Q301"  Part="2" 
AR Path="/61CC18AC/60C151A0" Ref="Q401"  Part="2" 
AR Path="/61D0F570/60C151A0" Ref="Q501"  Part="2" 
AR Path="/61D5D81A/60C151A0" Ref="Q?"  Part="2" 
AR Path="/61DAB55E/60C151A0" Ref="Q?"  Part="2" 
AR Path="/61DF933A/60C151A0" Ref="Q?"  Part="2" 
AR Path="/61E47116/60C151A0" Ref="Q?"  Part="2" 
AR Path="/61E94EE2/60C151A0" Ref="Q?"  Part="2" 
AR Path="/61EE2CE6/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E396/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E39F/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E3A8/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E3B1/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E3BA/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E3C3/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E3CC/60C151A0" Ref="Q?"  Part="2" 
AR Path="/6206E3D5/60C151A0" Ref="Q?"  Part="2" 
AR Path="/600B24BA/60C151A0" Ref="Q601"  Part="2" 
AR Path="/600B24BF/60C151A0" Ref="Q701"  Part="2" 
AR Path="/600B24C4/60C151A0" Ref="Q801"  Part="2" 
AR Path="/600B24C9/60C151A0" Ref="Q901"  Part="2" 
AR Path="/60734BB2/60C151A0" Ref="Q1001"  Part="2" 
AR Path="/60734BB7/60C151A0" Ref="Q1101"  Part="2" 
AR Path="/60734BBC/60C151A0" Ref="Q1201"  Part="2" 
AR Path="/60734BC1/60C151A0" Ref="Q1301"  Part="2" 
AR Path="/60734BC6/60C151A0" Ref="Q1401"  Part="2" 
AR Path="/60734BCB/60C151A0" Ref="Q1501"  Part="2" 
AR Path="/60734BD0/60C151A0" Ref="Q1601"  Part="2" 
AR Path="/60734BD5/60C151A0" Ref="Q1701"  Part="2" 
F 0 "Q201" H 4558 2346 50  0000 L CNN
F 1 "DMT3020LDV-7" H 4558 2255 50  0000 L CNN
F 2 "lib:TRANS_DMT3020LDV-7" H 4350 2300 50  0001 L BNN
F 3 "" H 4350 2300 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 4350 2300 50  0001 L BNN "STANDARD"
F 5 "2-2" H 4350 2300 50  0001 L BNN "PARTREV"
F 6 "DIODES Incorporated" H 4350 2300 50  0001 L BNN "MANUFACTURER"
F 7 "0.85 mm" H 4350 2300 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 8 "621-DMT3020LDV-7" H 4350 2300 50  0001 C CNN "Mouser"
	2    4350 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2200 4150 2400
Wire Wire Line
	4150 2400 4250 2400
Wire Wire Line
	4450 2500 4450 3100
Connection ~ 4450 3100
Wire Wire Line
	4450 3100 5200 3100
Connection ~ 5200 3100
Wire Wire Line
	5200 3100 5550 3100
Wire Wire Line
	3800 2050 4450 2050
Connection ~ 3800 2050
Wire Wire Line
	4450 2000 4450 2050
Connection ~ 4450 2050
Wire Wire Line
	4450 2050 4700 2050
Wire Wire Line
	4450 2050 4450 2100
Text HLabel 5850 2050 2    50   Output ~ 0
PWR
Wire Wire Line
	2000 3100 2300 3100
Connection ~ 2300 3100
Wire Wire Line
	2300 3100 3000 3100
Wire Wire Line
	1400 2100 2350 2100
Wire Wire Line
	1400 2000 2350 2000
Wire Wire Line
	2300 2200 2300 2500
Wire Wire Line
	2300 2800 2300 3100
Wire Wire Line
	3100 2600 3100 2700
Wire Wire Line
	3100 2700 3000 2700
Wire Wire Line
	3000 3100 3000 2700
Wire Wire Line
	3000 2700 3000 2600
Connection ~ 3000 2700
$Comp
L Device:R R201
U 1 1 5FDEF16F
P 2300 2650
AR Path="/5FBA8D0D/5FDEF16F" Ref="R201"  Part="1" 
AR Path="/619B6E22/5FDEF16F" Ref="R301"  Part="1" 
AR Path="/61CC18AC/5FDEF16F" Ref="R401"  Part="1" 
AR Path="/61D0F570/5FDEF16F" Ref="R501"  Part="1" 
AR Path="/600B24BA/5FDEF16F" Ref="R601"  Part="1" 
AR Path="/600B24BF/5FDEF16F" Ref="R701"  Part="1" 
AR Path="/600B24C4/5FDEF16F" Ref="R801"  Part="1" 
AR Path="/600B24C9/5FDEF16F" Ref="R901"  Part="1" 
AR Path="/60734BB2/5FDEF16F" Ref="R1001"  Part="1" 
AR Path="/60734BB7/5FDEF16F" Ref="R1101"  Part="1" 
AR Path="/60734BBC/5FDEF16F" Ref="R1201"  Part="1" 
AR Path="/60734BC1/5FDEF16F" Ref="R1301"  Part="1" 
AR Path="/60734BC6/5FDEF16F" Ref="R1401"  Part="1" 
AR Path="/60734BCB/5FDEF16F" Ref="R1501"  Part="1" 
AR Path="/60734BD0/5FDEF16F" Ref="R1601"  Part="1" 
AR Path="/60734BD5/5FDEF16F" Ref="R1701"  Part="1" 
F 0 "R201" H 2370 2696 50  0000 L CNN
F 1 "10k" H 2370 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2230 2650 50  0001 C CNN
F 3 "~" H 2300 2650 50  0001 C CNN
F 4 "652-CMP0603AFX-1002L" H 2300 2650 50  0001 C CNN "Mouser"
	1    2300 2650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
