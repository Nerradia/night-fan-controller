EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5750 6700 5750 6600
Connection ~ 5750 6600
Wire Wire Line
	5750 6600 5750 6500
$Comp
L Connector_Generic:Conn_01x08 J112
U 1 1 5FDA320C
P 4050 6650
F 0 "J112" H 4000 7200 50  0000 C CNN
F 1 "Power" H 4000 7100 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x08_P1.27mm_Vertical" H 4050 6650 50  0001 C CNN
F 3 "~" H 4050 6650 50  0001 C CNN
	1    4050 6650
	-1   0    0    -1  
$EndComp
Connection ~ 5750 7200
Wire Wire Line
	5750 7200 5750 7300
Wire Wire Line
	5750 7100 5750 7200
Wire Wire Line
	5750 6800 5750 6900
Connection ~ 5750 6900
Wire Wire Line
	5750 6900 5750 7000
Wire Wire Line
	5750 6000 5750 5900
Wire Wire Line
	5750 6200 5750 6300
Connection ~ 5750 6300
Wire Wire Line
	5750 6300 5750 6400
Wire Wire Line
	2500 4500 3000 4500
Wire Wire Line
	2500 4400 3000 4400
Wire Wire Line
	2500 4300 3000 4300
Wire Wire Line
	2500 4200 3000 4200
$Comp
L Connector_Generic:Conn_01x04 J106
U 1 1 5FDA325C
P 3200 4300
F 0 "J106" H 3250 4617 50  0000 C CNN
F 1 "Motherboard PWM" H 3250 4526 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 3200 4300 50  0001 C CNN
F 3 "~" H 3200 4300 50  0001 C CNN
	1    3200 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J103
U 1 1 5FDA3262
P 3200 3550
F 0 "J103" H 3250 3867 50  0000 C CNN
F 1 "Temp sensors" H 3250 3776 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 3200 3550 50  0001 C CNN
F 3 "~" H 3200 3550 50  0001 C CNN
	1    3200 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J101
U 1 1 5FDA3269
P 3200 2600
F 0 "J101" H 3280 2592 50  0000 L CNN
F 1 "LED_STRIPS" H 3280 2501 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x08_P1.27mm_Vertical" H 3200 2600 50  0001 C CNN
F 3 "~" H 3200 2600 50  0001 C CNN
	1    3200 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R101
U 1 1 5FDA328A
P 2700 5600
F 0 "R101" V 2493 5600 50  0000 C CNN
F 1 "100" V 2584 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2630 5600 50  0001 C CNN
F 3 "~" H 2700 5600 50  0001 C CNN
	1    2700 5600
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J109
U 1 1 5FDA3290
P 3200 5600
F 0 "J109" H 3280 5592 50  0000 L CNN
F 1 "Buzzer" H 3280 5501 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 3200 5600 50  0001 C CNN
F 3 "~" H 3200 5600 50  0001 C CNN
	1    3200 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5FDA3296
P 2900 5800
F 0 "#PWR0108" H 2900 5550 50  0001 C CNN
F 1 "GND" H 2905 5627 50  0000 C CNN
F 2 "" H 2900 5800 50  0001 C CNN
F 3 "" H 2900 5800 50  0001 C CNN
	1    2900 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 5800 2900 5700
Wire Wire Line
	2900 5700 3000 5700
Wire Wire Line
	2250 5600 2550 5600
Wire Wire Line
	2850 5600 3000 5600
Text Label 900  3950 0    50   ~ 0
PWM_1
Text Label 900  4050 0    50   ~ 0
PWM_2
Text Label 900  4150 0    50   ~ 0
PWM_3
Text Label 900  4250 0    50   ~ 0
PWM_4
Text Label 900  4350 0    50   ~ 0
PWM_5
Text Label 900  4450 0    50   ~ 0
PWM_6
Text Label 900  4550 0    50   ~ 0
PWM_7
Text Label 900  4650 0    50   ~ 0
PWM_8
Text Label 900  4750 0    50   ~ 0
PWM_9
Text Label 900  4850 0    50   ~ 0
PWM_10
Text Label 900  4950 0    50   ~ 0
PWM_11
Text Label 900  5050 0    50   ~ 0
PWM_12
Text Label 900  5150 0    50   ~ 0
PWM_13
Text Label 900  5250 0    50   ~ 0
PWM_14
Text Label 900  5350 0    50   ~ 0
PWM_15
Text Label 900  5450 0    50   ~ 0
PWM_16
Wire Wire Line
	1450 3950 900  3950
Wire Wire Line
	1450 4050 900  4050
Wire Wire Line
	1450 4150 900  4150
Wire Wire Line
	1450 4250 900  4250
Wire Wire Line
	1450 4350 900  4350
Wire Wire Line
	1450 4450 900  4450
Wire Wire Line
	1450 4550 900  4550
Wire Wire Line
	1450 4650 900  4650
Wire Wire Line
	1450 4750 900  4750
Wire Wire Line
	1450 4850 900  4850
Wire Wire Line
	1450 4950 900  4950
Wire Wire Line
	1450 5050 900  5050
Wire Wire Line
	1450 5150 900  5150
Wire Wire Line
	1450 5250 900  5250
Wire Wire Line
	1450 5350 900  5350
Wire Wire Line
	1450 5450 900  5450
Text Label 2250 5600 0    50   ~ 0
BUZZER
Text Label 900  1800 0    50   ~ 0
PWR_FAN_1
Text Label 900  1900 0    50   ~ 0
PWR_FAN_2
Text Label 900  2000 0    50   ~ 0
PWR_FAN_3
Text Label 900  2100 0    50   ~ 0
PWR_FAN_4
Text Label 900  2200 0    50   ~ 0
PWR_FAN_5
Text Label 900  2300 0    50   ~ 0
PWR_FAN_6
Text Label 900  2400 0    50   ~ 0
PWR_FAN_7
Text Label 900  2500 0    50   ~ 0
PWR_FAN_8
Text Label 900  2600 0    50   ~ 0
PWR_FAN_9
Text Label 900  2700 0    50   ~ 0
PWR_FAN_10
Text Label 900  2800 0    50   ~ 0
PWR_FAN_11
Text Label 900  2900 0    50   ~ 0
PWR_FAN_12
Text Label 900  3000 0    50   ~ 0
PWR_FAN_13
Text Label 900  3100 0    50   ~ 0
PWR_FAN_14
Text Label 900  3200 0    50   ~ 0
PWR_FAN_15
Text Label 900  3300 0    50   ~ 0
PWR_FAN_16
Wire Wire Line
	900  1800 1450 1800
Wire Wire Line
	900  1900 1450 1900
Wire Wire Line
	900  2000 1450 2000
Wire Wire Line
	900  2100 1450 2100
Wire Wire Line
	900  2200 1450 2200
Wire Wire Line
	900  2300 1450 2300
Wire Wire Line
	900  2400 1450 2400
Wire Wire Line
	900  2500 1450 2500
Wire Wire Line
	900  2600 1450 2600
Wire Wire Line
	900  2700 1450 2700
Wire Wire Line
	900  2800 1450 2800
Wire Wire Line
	900  2900 1450 2900
Wire Wire Line
	900  3000 1450 3000
Wire Wire Line
	900  3100 1450 3100
Wire Wire Line
	900  3200 1450 3200
Wire Wire Line
	900  3300 1450 3300
Text Label 2500 3450 0    50   ~ 0
TEMP_1
Text Label 2500 3550 0    50   ~ 0
TEMP_2
Text Label 2500 3650 0    50   ~ 0
TEMP_3
Text Label 2500 3750 0    50   ~ 0
TEMP_4
Wire Wire Line
	2500 3450 3000 3450
Wire Wire Line
	2500 3550 3000 3550
Wire Wire Line
	2500 3650 3000 3650
Wire Wire Line
	2500 3750 3000 3750
$Comp
L Connector_Generic:Conn_01x16 J107
U 1 1 5FDA32F1
P 1650 4650
F 0 "J107" H 1730 4642 50  0000 L CNN
F 1 "Conn_01x16" H 1730 4551 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x16_P1.27mm_Vertical" H 1650 4650 50  0001 C CNN
F 3 "~" H 1650 4650 50  0001 C CNN
	1    1650 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x16 J110
U 1 1 5FDA32F7
P 1650 6300
F 0 "J110" H 1730 6292 50  0000 L CNN
F 1 "Conn_01x16" H 1730 6201 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x16_P1.27mm_Vertical" H 1650 6300 50  0001 C CNN
F 3 "~" H 1650 6300 50  0001 C CNN
	1    1650 6300
	1    0    0    -1  
$EndComp
Text Label 2500 4200 0    50   ~ 0
MB_PWM_1
Text Label 2500 4300 0    50   ~ 0
MB_PWM_2
Text Label 2500 4400 0    50   ~ 0
MB_PWM_3
Text Label 2500 4500 0    50   ~ 0
MB_PWM_4
Text Label 2450 2300 0    50   ~ 0
LEDSTRIP_0
Wire Wire Line
	3000 2400 2450 2400
Wire Wire Line
	3000 2500 2450 2500
Wire Wire Line
	3000 2600 2450 2600
Text Label 2450 2400 0    50   ~ 0
LEDSTRIP_1
Text Label 2450 2500 0    50   ~ 0
LEDSTRIP_2
Text Label 2450 2600 0    50   ~ 0
LEDSTRIP_3
Text Label 2450 2700 0    50   ~ 0
LEDSTRIP_4
Text Label 2450 2800 0    50   ~ 0
LEDSTRIP_5
Text Label 2450 2900 0    50   ~ 0
LEDSTRIP_6
Text Label 2450 3000 0    50   ~ 0
LEDSTRIP_7
Wire Wire Line
	2450 2300 3000 2300
Wire Wire Line
	3000 2700 2450 2700
Wire Wire Line
	3000 2800 2450 2800
Wire Wire Line
	3000 2900 2450 2900
Wire Wire Line
	3000 3000 2450 3000
Text Notes 1050 1250 0    118  ~ 0
To connector board
Text Label 900  5600 0    50   ~ 0
TACH_1
Text Label 900  5700 0    50   ~ 0
TACH_2
Text Label 900  5800 0    50   ~ 0
TACH_3
Text Label 900  5900 0    50   ~ 0
TACH_4
Text Label 900  6000 0    50   ~ 0
TACH_5
Text Label 900  6100 0    50   ~ 0
TACH_6
Text Label 900  6200 0    50   ~ 0
TACH_7
Text Label 900  6300 0    50   ~ 0
TACH_8
Text Label 900  6400 0    50   ~ 0
TACH_9
Text Label 900  6500 0    50   ~ 0
TACH_10
Text Label 900  6600 0    50   ~ 0
TACH_11
Text Label 900  6700 0    50   ~ 0
TACH_12
Text Label 900  6800 0    50   ~ 0
TACH_13
Text Label 900  6900 0    50   ~ 0
TACH_14
Text Label 900  7000 0    50   ~ 0
TACH_15
Text Label 900  7100 0    50   ~ 0
TACH_16
Wire Wire Line
	1450 5600 900  5600
Wire Wire Line
	1450 5700 900  5700
Wire Wire Line
	1450 5800 900  5800
Wire Wire Line
	1450 5900 900  5900
Wire Wire Line
	1450 6000 900  6000
Wire Wire Line
	1450 6100 900  6100
Wire Wire Line
	1450 6200 900  6200
Wire Wire Line
	1450 6300 900  6300
Wire Wire Line
	1450 6400 900  6400
Wire Wire Line
	1450 6500 900  6500
Wire Wire Line
	1450 6600 900  6600
Wire Wire Line
	1450 6700 900  6700
Wire Wire Line
	1450 6800 900  6800
Wire Wire Line
	1450 6900 900  6900
Wire Wire Line
	1450 7000 900  7000
Wire Wire Line
	1450 7100 900  7100
$Comp
L Mechanical:MountingHole_Pad H101
U 1 1 5FDA3332
P 3850 950
F 0 "H101" H 3950 999 50  0000 L CNN
F 1 "MountingHole_Pad" H 3950 908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 3850 950 50  0001 C CNN
F 3 "~" H 3850 950 50  0001 C CNN
	1    3850 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5FDA3338
P 3850 1050
F 0 "#PWR0101" H 3850 800 50  0001 C CNN
F 1 "GND" H 3855 877 50  0000 C CNN
F 2 "" H 3850 1050 50  0001 C CNN
F 3 "" H 3850 1050 50  0001 C CNN
	1    3850 1050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H102
U 1 1 5FDA333E
P 4150 950
F 0 "H102" H 4250 999 50  0000 L CNN
F 1 "MountingHole_Pad" H 4250 908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4150 950 50  0001 C CNN
F 3 "~" H 4150 950 50  0001 C CNN
	1    4150 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5FDA3344
P 4150 1050
F 0 "#PWR0102" H 4150 800 50  0001 C CNN
F 1 "GND" H 4155 877 50  0000 C CNN
F 2 "" H 4150 1050 50  0001 C CNN
F 3 "" H 4150 1050 50  0001 C CNN
	1    4150 1050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H103
U 1 1 5FDA334A
P 4400 950
F 0 "H103" H 4500 999 50  0000 L CNN
F 1 "MountingHole_Pad" H 4500 908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4400 950 50  0001 C CNN
F 3 "~" H 4400 950 50  0001 C CNN
	1    4400 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5FDA3350
P 4400 1050
F 0 "#PWR0103" H 4400 800 50  0001 C CNN
F 1 "GND" H 4405 877 50  0000 C CNN
F 2 "" H 4400 1050 50  0001 C CNN
F 3 "" H 4400 1050 50  0001 C CNN
	1    4400 1050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H104
U 1 1 5FDA3356
P 4650 950
F 0 "H104" H 4750 999 50  0000 L CNN
F 1 "MountingHole_Pad" H 4750 908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4650 950 50  0001 C CNN
F 3 "~" H 4650 950 50  0001 C CNN
	1    4650 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5FDA335C
P 4650 1050
F 0 "#PWR0104" H 4650 800 50  0001 C CNN
F 1 "GND" H 4655 877 50  0000 C CNN
F 2 "" H 4650 1050 50  0001 C CNN
F 3 "" H 4650 1050 50  0001 C CNN
	1    4650 1050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x24 J102
U 1 1 5FDA3362
P 1650 2500
F 0 "J102" H 1730 2492 50  0000 L CNN
F 1 "Conn_01x24" H 1730 2401 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x24_P1.27mm_Vertical" H 1650 2500 50  0001 C CNN
F 3 "~" H 1650 2500 50  0001 C CNN
	1    1650 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5FDA3368
P 1150 3550
F 0 "#PWR0106" H 1150 3300 50  0001 C CNN
F 1 "GND" V 1155 3422 50  0000 R CNN
F 2 "" H 1150 3550 50  0001 C CNN
F 3 "" H 1150 3550 50  0001 C CNN
	1    1150 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 3550 1300 3550
Wire Wire Line
	1300 3550 1300 3500
Wire Wire Line
	1300 3400 1450 3400
Wire Wire Line
	1450 3500 1300 3500
Connection ~ 1300 3500
Wire Wire Line
	1300 3500 1300 3400
Wire Wire Line
	1300 3550 1300 3600
Wire Wire Line
	1300 3700 1450 3700
Connection ~ 1300 3550
Wire Wire Line
	1450 3600 1300 3600
Connection ~ 1300 3600
Wire Wire Line
	1300 3600 1300 3700
$Comp
L power:GND #PWR0105
U 1 1 5FDA337A
P 1150 1550
F 0 "#PWR0105" H 1150 1300 50  0001 C CNN
F 1 "GND" V 1155 1422 50  0000 R CNN
F 2 "" H 1150 1550 50  0001 C CNN
F 3 "" H 1150 1550 50  0001 C CNN
	1    1150 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 1550 1300 1550
Wire Wire Line
	1300 1550 1300 1500
Wire Wire Line
	1300 1400 1450 1400
Wire Wire Line
	1450 1500 1300 1500
Connection ~ 1300 1500
Wire Wire Line
	1300 1500 1300 1400
Wire Wire Line
	1300 1550 1300 1600
Wire Wire Line
	1300 1700 1450 1700
Connection ~ 1300 1550
Wire Wire Line
	1450 1600 1300 1600
Connection ~ 1300 1600
Wire Wire Line
	1300 1600 1300 1700
$Comp
L Connector_Generic:Conn_01x15 J140
U 1 1 5FD05388
P 6050 6600
F 0 "J140" H 5968 7517 50  0000 C CNN
F 1 "Power" H 5968 7426 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x15_P1.27mm_Vertical" H 6050 6600 50  0001 C CNN
F 3 "~" H 6050 6600 50  0001 C CNN
	1    6050 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 6000 5850 6000
Connection ~ 5750 6000
Wire Wire Line
	5750 5900 5850 5900
Wire Wire Line
	5750 6200 5850 6200
Wire Wire Line
	5850 6400 5750 6400
Wire Wire Line
	5750 6300 5850 6300
Wire Wire Line
	5750 6500 5850 6500
Wire Wire Line
	5750 6600 5850 6600
Wire Wire Line
	5750 6700 5850 6700
Wire Wire Line
	5750 6800 5850 6800
Wire Wire Line
	5750 6900 5850 6900
Wire Wire Line
	5850 7000 5750 7000
Wire Wire Line
	5750 7100 5850 7100
Wire Wire Line
	5850 7200 5750 7200
Wire Wire Line
	5750 7300 5850 7300
$Comp
L Connector_Generic:Conn_01x04 J120
U 1 1 5FD721A6
P 8050 1750
F 0 "J120" H 8130 1742 50  0000 L CNN
F 1 "FAN_1" H 8130 1651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 1750 50  0001 C CNN
F 3 "~" H 8050 1750 50  0001 C CNN
F 4 "171857-0004" H 8050 1750 50  0001 C CNN "Ref mouser"
	1    8050 1750
	1    0    0    -1  
$EndComp
Text Label 7350 1750 0    50   ~ 0
PWR_FAN_1
Text Label 7350 2250 0    50   ~ 0
PWR_FAN_2
Text Label 7350 2750 0    50   ~ 0
PWR_FAN_3
Text Label 7350 3250 0    50   ~ 0
PWR_FAN_4
Text Label 7350 3750 0    50   ~ 0
PWR_FAN_5
Text Label 7350 4250 0    50   ~ 0
PWR_FAN_6
Text Label 7350 4750 0    50   ~ 0
PWR_FAN_7
Text Label 7350 5250 0    50   ~ 0
PWR_FAN_8
Text Label 8750 1750 0    50   ~ 0
PWR_FAN_9
Text Label 8750 2250 0    50   ~ 0
PWR_FAN_10
Text Label 8750 2750 0    50   ~ 0
PWR_FAN_11
Text Label 8750 3250 0    50   ~ 0
PWR_FAN_12
Text Label 8750 3750 0    50   ~ 0
PWR_FAN_13
Text Label 8750 4250 0    50   ~ 0
PWR_FAN_14
Text Label 8750 4750 0    50   ~ 0
PWR_FAN_15
Text Label 8750 5250 0    50   ~ 0
PWR_FAN_16
Text Label 7350 1850 0    50   ~ 0
TACH_1
Text Label 7350 2350 0    50   ~ 0
TACH_2
Text Label 7350 2850 0    50   ~ 0
TACH_3
Text Label 7350 3350 0    50   ~ 0
TACH_4
Text Label 7350 3850 0    50   ~ 0
TACH_5
Text Label 7350 4350 0    50   ~ 0
TACH_6
Text Label 7350 4850 0    50   ~ 0
TACH_7
Text Label 7350 5350 0    50   ~ 0
TACH_8
Text Label 8750 1850 0    50   ~ 0
TACH_9
Text Label 8750 2350 0    50   ~ 0
TACH_10
Text Label 8750 2850 0    50   ~ 0
TACH_11
Text Label 8750 3350 0    50   ~ 0
TACH_12
Text Label 8750 3850 0    50   ~ 0
TACH_13
Text Label 8750 4350 0    50   ~ 0
TACH_14
Text Label 8750 4850 0    50   ~ 0
TACH_15
Text Label 8750 5350 0    50   ~ 0
TACH_16
Text Label 7350 1950 0    50   ~ 0
PWM_1
Text Label 7350 2450 0    50   ~ 0
PWM_2
Text Label 7350 2950 0    50   ~ 0
PWM_3
Text Label 7350 3450 0    50   ~ 0
PWM_4
Text Label 7350 3950 0    50   ~ 0
PWM_5
Text Label 7350 4450 0    50   ~ 0
PWM_6
Text Label 7350 4950 0    50   ~ 0
PWM_7
Text Label 7350 5450 0    50   ~ 0
PWM_8
Text Label 8750 1950 0    50   ~ 0
PWM_9
Text Label 8750 2450 0    50   ~ 0
PWM_10
Text Label 8750 2950 0    50   ~ 0
PWM_11
Text Label 8750 3450 0    50   ~ 0
PWM_12
Text Label 8750 3950 0    50   ~ 0
PWM_13
Text Label 8750 4450 0    50   ~ 0
PWM_14
Text Label 8750 4950 0    50   ~ 0
PWM_15
Text Label 8750 5450 0    50   ~ 0
PWM_16
Text Label 7350 1650 0    50   ~ 0
GND
Text Label 7350 2150 0    50   ~ 0
GND
Text Label 7350 2650 0    50   ~ 0
GND
Text Label 7350 3150 0    50   ~ 0
GND
Text Label 7350 3650 0    50   ~ 0
GND
Text Label 7350 4150 0    50   ~ 0
GND
Text Label 7350 4650 0    50   ~ 0
GND
Text Label 7350 5150 0    50   ~ 0
GND
Text Label 8750 1650 0    50   ~ 0
GND
Text Label 8750 2150 0    50   ~ 0
GND
Text Label 8750 2650 0    50   ~ 0
GND
Text Label 8750 3150 0    50   ~ 0
GND
Text Label 8750 3650 0    50   ~ 0
GND
Text Label 8750 4150 0    50   ~ 0
GND
Text Label 8750 4650 0    50   ~ 0
GND
Text Label 8750 5150 0    50   ~ 0
GND
Wire Wire Line
	7350 1650 7850 1650
Wire Wire Line
	7350 2150 7850 2150
Wire Wire Line
	7350 2650 7850 2650
Wire Wire Line
	7350 3150 7850 3150
Wire Wire Line
	7350 3650 7850 3650
Wire Wire Line
	7350 4150 7850 4150
Wire Wire Line
	7350 4650 7850 4650
Wire Wire Line
	7350 5150 7850 5150
Wire Wire Line
	8750 1650 9250 1650
Wire Wire Line
	8750 2150 9250 2150
Wire Wire Line
	8750 2650 9250 2650
Wire Wire Line
	8750 3150 9250 3150
Wire Wire Line
	8750 3650 9250 3650
Wire Wire Line
	8750 4150 9250 4150
Wire Wire Line
	8750 4650 9250 4650
Wire Wire Line
	8750 5150 9250 5150
Wire Wire Line
	7350 1750 7850 1750
Wire Wire Line
	7350 2250 7850 2250
Wire Wire Line
	7350 2750 7850 2750
Wire Wire Line
	7350 3250 7850 3250
Wire Wire Line
	7350 3750 7850 3750
Wire Wire Line
	7350 4250 7850 4250
Wire Wire Line
	7350 4750 7850 4750
Wire Wire Line
	7350 5250 7850 5250
Wire Wire Line
	8750 1750 9250 1750
Wire Wire Line
	8750 2250 9250 2250
Wire Wire Line
	8750 2750 9250 2750
Wire Wire Line
	8750 3250 9250 3250
Wire Wire Line
	8750 3750 9250 3750
Wire Wire Line
	8750 4250 9250 4250
Wire Wire Line
	8750 4750 9250 4750
Wire Wire Line
	8750 5250 9250 5250
Wire Wire Line
	7350 1850 7850 1850
Wire Wire Line
	7350 2350 7850 2350
Wire Wire Line
	7350 2850 7850 2850
Wire Wire Line
	7350 3350 7850 3350
Wire Wire Line
	7350 3850 7850 3850
Wire Wire Line
	7350 4350 7850 4350
Wire Wire Line
	7350 4850 7850 4850
Wire Wire Line
	7350 5350 7850 5350
Wire Wire Line
	8750 1850 9250 1850
Wire Wire Line
	8750 2350 9250 2350
Wire Wire Line
	8750 2850 9250 2850
Wire Wire Line
	8750 3350 9250 3350
Wire Wire Line
	8750 3850 9250 3850
Wire Wire Line
	8750 4350 9250 4350
Wire Wire Line
	8750 4850 9250 4850
Wire Wire Line
	8750 5350 9250 5350
Wire Wire Line
	7350 1950 7850 1950
Wire Wire Line
	7350 2450 7850 2450
Wire Wire Line
	7350 2950 7850 2950
Wire Wire Line
	7350 3450 7850 3450
Wire Wire Line
	7350 3950 7850 3950
Wire Wire Line
	7350 4450 7850 4450
Wire Wire Line
	7350 4950 7850 4950
Wire Wire Line
	7350 5450 7850 5450
Wire Wire Line
	8750 1950 9250 1950
Wire Wire Line
	8750 2450 9250 2450
Wire Wire Line
	8750 2950 9250 2950
Wire Wire Line
	8750 3450 9250 3450
Wire Wire Line
	8750 3950 9250 3950
Wire Wire Line
	8750 4450 9250 4450
Wire Wire Line
	8750 4950 9250 4950
Wire Wire Line
	8750 5450 9250 5450
$Comp
L Connector_Generic:Conn_01x04 J121
U 1 1 60216CF8
P 8050 2250
F 0 "J121" H 8130 2242 50  0000 L CNN
F 1 "FAN_2" H 8130 2151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 2250 50  0001 C CNN
F 3 "~" H 8050 2250 50  0001 C CNN
F 4 "171857-0004" H 8050 2250 50  0001 C CNN "Ref mouser"
	1    8050 2250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J122
U 1 1 602174A1
P 8050 2750
F 0 "J122" H 8130 2742 50  0000 L CNN
F 1 "FAN_3" H 8130 2651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 2750 50  0001 C CNN
F 3 "~" H 8050 2750 50  0001 C CNN
F 4 "171857-0004" H 8050 2750 50  0001 C CNN "Ref mouser"
	1    8050 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J123
U 1 1 60217B46
P 8050 3250
F 0 "J123" H 8130 3242 50  0000 L CNN
F 1 "FAN_4" H 8130 3151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 3250 50  0001 C CNN
F 3 "~" H 8050 3250 50  0001 C CNN
F 4 "171857-0004" H 8050 3250 50  0001 C CNN "Ref mouser"
	1    8050 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J124
U 1 1 6021D634
P 8050 3750
F 0 "J124" H 8130 3742 50  0000 L CNN
F 1 "FAN_5" H 8130 3651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 3750 50  0001 C CNN
F 3 "~" H 8050 3750 50  0001 C CNN
F 4 "171857-0004" H 8050 3750 50  0001 C CNN "Ref mouser"
	1    8050 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J125
U 1 1 6021D63B
P 8050 4250
F 0 "J125" H 8130 4242 50  0000 L CNN
F 1 "FAN_6" H 8130 4151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 4250 50  0001 C CNN
F 3 "~" H 8050 4250 50  0001 C CNN
F 4 "171857-0004" H 8050 4250 50  0001 C CNN "Ref mouser"
	1    8050 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J126
U 1 1 6021D642
P 8050 4750
F 0 "J126" H 8130 4742 50  0000 L CNN
F 1 "FAN_7" H 8130 4651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 4750 50  0001 C CNN
F 3 "~" H 8050 4750 50  0001 C CNN
F 4 "171857-0004" H 8050 4750 50  0001 C CNN "Ref mouser"
	1    8050 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J127
U 1 1 6021D649
P 8050 5250
F 0 "J127" H 8130 5242 50  0000 L CNN
F 1 "FAN_8" H 8130 5151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8050 5250 50  0001 C CNN
F 3 "~" H 8050 5250 50  0001 C CNN
F 4 "171857-0004" H 8050 5250 50  0001 C CNN "Ref mouser"
	1    8050 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J128
U 1 1 6022F96D
P 9450 1750
F 0 "J128" H 9530 1742 50  0000 L CNN
F 1 "FAN_9" H 9530 1651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 1750 50  0001 C CNN
F 3 "~" H 9450 1750 50  0001 C CNN
F 4 "171857-0004" H 9450 1750 50  0001 C CNN "Ref mouser"
	1    9450 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J129
U 1 1 6022F974
P 9450 2250
F 0 "J129" H 9530 2242 50  0000 L CNN
F 1 "FAN_10" H 9530 2151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 2250 50  0001 C CNN
F 3 "~" H 9450 2250 50  0001 C CNN
F 4 "171857-0004" H 9450 2250 50  0001 C CNN "Ref mouser"
	1    9450 2250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J130
U 1 1 6022F97B
P 9450 2750
F 0 "J130" H 9530 2742 50  0000 L CNN
F 1 "FAN_11" H 9530 2651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 2750 50  0001 C CNN
F 3 "~" H 9450 2750 50  0001 C CNN
F 4 "171857-0004" H 9450 2750 50  0001 C CNN "Ref mouser"
	1    9450 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J131
U 1 1 6022F982
P 9450 3250
F 0 "J131" H 9530 3242 50  0000 L CNN
F 1 "FAN_12" H 9530 3151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 3250 50  0001 C CNN
F 3 "~" H 9450 3250 50  0001 C CNN
F 4 "171857-0004" H 9450 3250 50  0001 C CNN "Ref mouser"
	1    9450 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J132
U 1 1 60240D98
P 9450 3750
F 0 "J132" H 9530 3742 50  0000 L CNN
F 1 "FAN_13" H 9530 3651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 3750 50  0001 C CNN
F 3 "~" H 9450 3750 50  0001 C CNN
F 4 "171857-0004" H 9450 3750 50  0001 C CNN "Ref mouser"
	1    9450 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J133
U 1 1 60240D9F
P 9450 4250
F 0 "J133" H 9530 4242 50  0000 L CNN
F 1 "FAN_14" H 9530 4151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 4250 50  0001 C CNN
F 3 "~" H 9450 4250 50  0001 C CNN
F 4 "171857-0004" H 9450 4250 50  0001 C CNN "Ref mouser"
	1    9450 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J134
U 1 1 60240DA6
P 9450 4750
F 0 "J134" H 9530 4742 50  0000 L CNN
F 1 "FAN_15" H 9530 4651 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 4750 50  0001 C CNN
F 3 "~" H 9450 4750 50  0001 C CNN
F 4 "171857-0004" H 9450 4750 50  0001 C CNN "Ref mouser"
	1    9450 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J135
U 1 1 60240DAD
P 9450 5250
F 0 "J135" H 9530 5242 50  0000 L CNN
F 1 "FAN_16" H 9530 5151 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9450 5250 50  0001 C CNN
F 3 "~" H 9450 5250 50  0001 C CNN
F 4 "171857-0004" H 9450 5250 50  0001 C CNN "Ref mouser"
	1    9450 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 6000 5750 6100
Wire Wire Line
	5750 6100 5850 6100
Text Label 5300 6000 0    50   ~ 0
+3.3V
Wire Wire Line
	5300 6000 5750 6000
$Comp
L power:GND #PWR0107
U 1 1 5FEA1B3A
P 5450 6300
F 0 "#PWR0107" H 5450 6050 50  0001 C CNN
F 1 "GND" V 5455 6172 50  0000 R CNN
F 2 "" H 5450 6300 50  0001 C CNN
F 3 "" H 5450 6300 50  0001 C CNN
	1    5450 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 6300 5750 6300
Text Label 5300 6600 0    50   ~ 0
+5V
$Comp
L power:GND #PWR0109
U 1 1 5FEC8222
P 5450 6900
F 0 "#PWR0109" H 5450 6650 50  0001 C CNN
F 1 "GND" V 5455 6772 50  0000 R CNN
F 2 "" H 5450 6900 50  0001 C CNN
F 3 "" H 5450 6900 50  0001 C CNN
	1    5450 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 6900 5750 6900
Wire Wire Line
	5300 6600 5750 6600
Text Label 5300 7200 0    50   ~ 0
+12V
Wire Wire Line
	5300 7200 5750 7200
Text Label 4700 6950 2    50   ~ 0
+12V
Wire Wire Line
	4700 6950 4350 6950
Wire Wire Line
	4250 6850 4350 6850
Wire Wire Line
	4350 6850 4350 6950
Wire Wire Line
	4350 7050 4250 7050
Connection ~ 4350 6950
Wire Wire Line
	4350 6950 4250 6950
Wire Wire Line
	4350 6950 4350 7050
Wire Wire Line
	4350 6750 4350 6650
Connection ~ 4350 6650
Wire Wire Line
	4350 6650 4350 6550
Wire Wire Line
	4350 6750 4250 6750
Wire Wire Line
	4350 6650 4250 6650
Wire Wire Line
	4250 6550 4350 6550
$Comp
L power:GND #PWR0110
U 1 1 5FF1E53F
P 4650 6650
F 0 "#PWR0110" H 4650 6400 50  0001 C CNN
F 1 "GND" V 4655 6522 50  0000 R CNN
F 2 "" H 4650 6650 50  0001 C CNN
F 3 "" H 4650 6650 50  0001 C CNN
	1    4650 6650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 6650 4350 6650
Text Label 4700 6450 2    50   ~ 0
+5V
Wire Wire Line
	4700 6450 4250 6450
Text Label 4700 6350 2    50   ~ 0
+3.3V
Wire Wire Line
	4700 6350 4250 6350
Text Label 4400 4300 0    50   ~ 0
MB_PWM_1
Text Label 4400 4400 0    50   ~ 0
MB_PWM_2
Text Label 4400 4500 0    50   ~ 0
MB_PWM_3
Text Label 4400 4600 0    50   ~ 0
MB_PWM_4
Text Label 4400 3500 0    50   ~ 0
TEMP_1
Text Label 4400 3600 0    50   ~ 0
TEMP_2
Text Label 4400 3700 0    50   ~ 0
TEMP_3
Text Label 4400 3800 0    50   ~ 0
TEMP_4
Text Label 5650 2300 2    50   ~ 0
LEDSTRIP_0
Wire Wire Line
	5100 2400 5650 2400
Wire Wire Line
	5100 2500 5650 2500
Wire Wire Line
	5100 2600 5650 2600
Text Label 5650 2400 2    50   ~ 0
LEDSTRIP_1
Text Label 5650 2500 2    50   ~ 0
LEDSTRIP_2
Text Label 5650 2600 2    50   ~ 0
LEDSTRIP_3
Text Label 5650 2700 2    50   ~ 0
LEDSTRIP_4
Text Label 5650 2800 2    50   ~ 0
LEDSTRIP_5
Text Label 5650 2900 2    50   ~ 0
LEDSTRIP_6
Text Label 5650 3000 2    50   ~ 0
LEDSTRIP_7
Wire Wire Line
	5650 2300 5100 2300
Wire Wire Line
	5100 2700 5650 2700
Wire Wire Line
	5100 2800 5650 2800
Wire Wire Line
	5100 2900 5650 2900
Wire Wire Line
	5100 3000 5650 3000
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J104
U 1 1 5FFD9E82
P 4800 2600
F 0 "J104" H 4850 3117 50  0000 C CNN
F 1 "LED_STRIPS" H 4850 3026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x08_P2.54mm_Vertical" H 4800 2600 50  0001 C CNN
F 3 "~" H 4800 2600 50  0001 C CNN
	1    4800 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J108
U 1 1 5FFDC188
P 5150 3600
F 0 "J108" H 5200 3917 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 5200 3826 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 5150 3600 50  0001 C CNN
F 3 "~" H 5150 3600 50  0001 C CNN
	1    5150 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3500 4950 3500
Wire Wire Line
	4400 3600 4950 3600
Wire Wire Line
	4400 3700 4950 3700
Wire Wire Line
	4400 3800 4950 3800
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J111
U 1 1 60020E1A
P 5150 4400
F 0 "J111" H 5200 4717 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 5200 4626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 5150 4400 50  0001 C CNN
F 3 "~" H 5150 4400 50  0001 C CNN
	1    5150 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4300 4950 4300
Wire Wire Line
	4400 4400 4950 4400
Wire Wire Line
	4400 4500 4950 4500
Wire Wire Line
	4400 4600 4950 4600
$Comp
L Connector_Generic:Conn_01x08 J105
U 1 1 60065494
P 5900 2600
F 0 "J105" H 5818 3117 50  0000 C CNN
F 1 "LED_STRIPS_GND" H 5818 3026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5900 2600 50  0001 C CNN
F 3 "~" H 5900 2600 50  0001 C CNN
	1    5900 2600
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 600AA915
P 6200 3100
F 0 "#PWR0111" H 6200 2850 50  0001 C CNN
F 1 "GND" H 6205 2927 50  0000 C CNN
F 2 "" H 6200 3100 50  0001 C CNN
F 3 "" H 6200 3100 50  0001 C CNN
	1    6200 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3100 6200 3000
Wire Wire Line
	6200 2300 6100 2300
Wire Wire Line
	6100 2400 6200 2400
Connection ~ 6200 2400
Wire Wire Line
	6200 2400 6200 2300
Wire Wire Line
	6100 2500 6200 2500
Connection ~ 6200 2500
Wire Wire Line
	6200 2500 6200 2400
Wire Wire Line
	6100 2600 6200 2600
Connection ~ 6200 2600
Wire Wire Line
	6200 2600 6200 2500
Wire Wire Line
	6100 2700 6200 2700
Connection ~ 6200 2700
Wire Wire Line
	6200 2700 6200 2600
Wire Wire Line
	6100 2800 6200 2800
Connection ~ 6200 2800
Wire Wire Line
	6200 2800 6200 2700
Wire Wire Line
	6100 2900 6200 2900
Connection ~ 6200 2900
Wire Wire Line
	6200 2900 6200 2800
Wire Wire Line
	6100 3000 6200 3000
Connection ~ 6200 3000
Wire Wire Line
	6200 3000 6200 2900
Text Label 4250 2300 0    50   ~ 0
+5V
Wire Wire Line
	4250 2300 4500 2300
Wire Wire Line
	4500 3000 4500 2900
Wire Wire Line
	4500 3000 4600 3000
Connection ~ 4500 2300
Wire Wire Line
	4500 2300 4600 2300
Wire Wire Line
	4500 2400 4600 2400
Connection ~ 4500 2400
Wire Wire Line
	4500 2400 4500 2300
Wire Wire Line
	4500 2500 4600 2500
Connection ~ 4500 2500
Wire Wire Line
	4500 2500 4500 2400
Wire Wire Line
	4500 2600 4600 2600
Connection ~ 4500 2600
Wire Wire Line
	4500 2600 4500 2500
Wire Wire Line
	4500 2700 4600 2700
Connection ~ 4500 2700
Wire Wire Line
	4500 2700 4500 2600
Wire Wire Line
	4500 2800 4600 2800
Connection ~ 4500 2800
Wire Wire Line
	4500 2800 4500 2700
Wire Wire Line
	4500 2900 4600 2900
Connection ~ 4500 2900
Wire Wire Line
	4500 2900 4500 2800
$Comp
L power:GND #PWR?
U 1 1 6032CCD2
P 5550 4700
F 0 "#PWR?" H 5550 4450 50  0001 C CNN
F 1 "GND" H 5555 4527 50  0000 C CNN
F 2 "" H 5550 4700 50  0001 C CNN
F 3 "" H 5550 4700 50  0001 C CNN
	1    5550 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4700 5550 4600
Wire Wire Line
	5450 4300 5550 4300
Connection ~ 5550 4300
Wire Wire Line
	5450 4400 5550 4400
Connection ~ 5550 4400
Wire Wire Line
	5550 4400 5550 4300
Wire Wire Line
	5450 4500 5550 4500
Connection ~ 5550 4500
Wire Wire Line
	5550 4500 5550 4400
Wire Wire Line
	5450 4600 5550 4600
Connection ~ 5550 4600
Wire Wire Line
	5550 4600 5550 4500
Wire Wire Line
	5450 3500 5550 3500
Wire Wire Line
	5450 3600 5550 3600
Connection ~ 5550 3600
Wire Wire Line
	5550 3600 5550 3500
Wire Wire Line
	5450 3700 5550 3700
Connection ~ 5550 3700
Wire Wire Line
	5550 3700 5550 3600
Wire Wire Line
	5450 3800 5550 3800
Connection ~ 5550 3800
Wire Wire Line
	5550 3800 5550 3700
Wire Wire Line
	5550 3800 5550 4300
$EndSCHEMATC
